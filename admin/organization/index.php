<?php
require("../models/Location.php");
require("../layout/index.php");

if (isset($_POST["categoryOrganization"])) {
    $name = $_POST["orgName"];
    $address = $_POST["orgAddress"];
    $phone = $_POST["orgPhone"];
    createOrganization($name, $address, $phone);
}

if (isset($_POST["btnDelete"])) {
    $id = $_POST['id'];
    deleteOrganization($id);
}

$locations = getAllLocation();
?>

<meta charset="UTF-8">
<title>Location</title>

<script src="../../public/js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">
<script type="text/javascript" src="../../public/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/organization.css">

<div class="content">

    <div class="box-header col-md-12 col-sm-12" style="margin-bottom: 10px;">
        <div class="col-md-5 col-sm-5"><a href="../site/index.php" style="margin-left: -15px">
                <h2 class="blue" style=" margin-left: -15px">
                    <span class=" glyphicon glyphicon-chevron-left"></span>Home &nbsp;</h2>
            </a>
        </div>
        <div class="col-md-5 col-sm-5" style="margin-left: 15px;"><h2 class="blue">Locations &nbsp;</h2></div>
        <div class="col-md-1 col-sm-1">
            <a href="#" data-toggle="modal" data-target="#myModal">
                <h2 class="blue" style=" margin-left: 160px">
                    <span data-toggle="tooltip" title="Add" data-placement="left"
                          class="glyphicon glyphicon-plus"></span>
                </h2>
            </a>
        </div>

    </div>
    <div class="box-content">

        <div class="col-md-12" style="margin: -10px 0px 0px 3px ">
            <table class="table table-striped table-bordered" id="list-orders" style="font-size: small">
                <thead>
                <tr>
                    <th style="text-align: center" width="20%">Name</th>
                    <th style="text-align: center" width="30%">Address</th>
                    <th style="text-align: center" width="20%">Phone</th>
                    <th style="text-align: center" width="10%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($locations); $i++) { ?>
                    <tr>
                        <td><?= $locations[$i]['org_name'] ?></td>
                        <td style="text-align: center"><?= $locations[$i]['address'] ?></td>
                        <td style="text-align: center"><?= $locations[$i]['phone'] ?></td>
                        <td style="text-align: center">
                            <form method="post" action="#" id="delete-form" style="display: inline">
                                <input type="hidden" name="id" value="<?= $locations[$i]['org_id']; ?>">

                                <button class="btn btn-danger" name="btnDelete" type="submit" style="font-size: x-small"
                                        value="delete"
                                        onclick="return confirm('Are you sure to delete?');">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Organization</h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post">

                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" class="form-control" name="orgName">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" name="orgAddress">
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" name="orgPhone">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" style="border-radius: 0" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" name="categoryOrganization"
                        style="border-radius: 0" class="btn btn-primary">Add Organization</button>
            </div>
                </form>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $(".employee-select").select2({
            placeholder: "Select a category",
            allowClear: true
        });
        $('#list-orders').DataTable();
    });

</script>