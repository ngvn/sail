<?php
require("../models/Order.php");
require("../layout/index.php");

global $conn;
//str_replace('.', '-', $_POST["a"])
$orders = getAllOrders();

?>


<meta charset="UTF-8">
<title>Orders</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">
<script type="text/javascript" src="../../public/js/jquery.dataTables.min.js"></script>
<script src="../../public/js/owl.carousel.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../public/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="../../public/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="../css/order.css">


<div class="content">
    <div class="product-list">


        <div id="product">
            <div class="box-header" style="margin-bottom: 10px;">
                <a href="../pos/index.php" class="col-md-6" style="margin-left: -15px"><h2 class="blue"><span
                            class="glyphicon glyphicon-chevron-left"></span>POS &nbsp;</h2>
                </a>

                <div class="col-md-5" style="margin-left: 0px;"><h2 class="blue">Orders &nbsp;</h2></div>
                <div class="col-md-1 col-sm-1"><h2 class="blue" style=" margin-left: 80px">
                <span data-toggle="tooltip" title="Export" data-placement="left"
                      class="glyphicon glyphicon-export"></span>
                    </h2></div>


            </div>
            <div class="box-content">
                <div class="input-control text" id="from">
                    <input type="text" placeholder="From Date" id="from-date" name="a">
                    <button class="button"><span class="glyphicon glyphicon-calendar"></span></button>
                </div>

                <div class="input-control text" id="to">
                    <input type="text" placeholder="To Date" id="to-date" name="a">
                    <button class="button"><span class="glyphicon glyphicon-calendar" style=""></span></button>
                </div>
                <table class="table table-striped table-bordered" id="list-products">
                    <thead>
                    <tr>
                        <th style="text-align: center">Code Order</th>
                        <th style="text-align: center">Date</th>
                        <th style="text-align: center">Customer</th>
                        <th style="text-align: center">SaleMan</th>
                        <th style="text-align: center">Discount</th>
                        <th style="text-align: center">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < count($orders); $i++) { ?>
                        <tr>
                            <td><?php echo $orders[$i]['order_id']; ?></td>
                            <td><?php echo $orders[$i]['date_created']; ?></td>
                            <td><?php echo getCustomerOrder($orders[$i]['business_partner_id']) ?></td>
                            <td><?php echo getSaleManOrder($orders[$i]['salesman_id']); ?></td>
                            <td><?php echo $orders[$i]['discount'] ?></td>
                            <td><?php echo($orders[$i]['total']) ?></td>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });

        $('#list-products').DataTable();
        $(".popover-menu").click(function () {
            $(".menu-content").toggle();
        });

        $("#btn-zoom").click(function () {
            $("label").toggle();
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
    $(function () {
        $("#from").datepicker();
        $("#to").datepicker();
    });
</script>
