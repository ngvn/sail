<?php
require("../models/User.php");
require("../layout/index.php");

$id = $_SESSION["idUser"];
$user = getInfoUserById($id);
?>


<title>Change Profile</title>
<meta charset="UTF-8">
<link href="../../public/css/metro.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/user.css">
<script src="../../public/js/select2.min.js"></script>


<div class="content" style="">
    <div class="box-header">
        <a href="index.php" class="col-xs-6" style="margin-left: -15px"><h2 class="blue"><span
                    class="glyphicon glyphicon-chevron-left"></span>Employees &nbsp;</h2>
        </a>

        <div href="#" class="col-xs-6"><h2 class="blue">Change Profile &nbsp;</h2></div>


    </div>

    <div class="box-content" id="box-content" style="padding: 20px;">
        <div class="">
            <p class="introtext"><span style="margin-left: 40px">Please fill in the information below. The field labels marked with * are required input
                fields.</span></p>
        </div>
        <div class="col-md-2"></div>
        <form method="post" action="index.php" enctype="multipart/form-data" class="col-md-8">
            <div class="col-xs-5">
                <div class="form-group all">
                    <label id="img-avatar">
                        <input type="file" name="avatarEdit" style="display: none;" id="avatar">
                        <img id="blah" class="img-default"
                             src="<?= (isset($user['url'])) ? $user['url'] : '../../upload/img/img-upload.png' ?>"
                             title="avatar"
                             alt="your image" width="100%" height="100%"/>
                    </label>
                </div>
            </div>
            <div class="col-xs-7">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="nameEdit" placeholder="Name *"
                           value="<?= (isset($user['name'])) ? $user['name'] : '' ?>" required="required"
                           class="form-control" id="name-Edit">
                </div>
                <div class="form-group">
                    <label>UserName</label>
                    <input type="text" name="userEdit" placeholder="UserName *"
                           value="<?= (isset($user['user_name'])) ? $user['user_name'] : '' ?>" disabled
                           class="form-control" id="user-Edit">
                </div>
                <div class="form-group">
                    <label>Password</label>&nbsp;&nbsp;&nbsp;&nbsp;
                    <label data-toggle="modal" data-target="#myModal" style="color: orangered"
                           title="Click to change password!">
                        <span class="glyphicon glyphicon-pencil"></span>Change Password
                    </label>
                    <input type="password" name="passEdit" placeholder="Pass *"
                           value="**********" disabled
                           class="form-control" id="user-Edit">
                </div>
                <div class="form-group">
                    <label>Birthday</label><br>

                    <div class="input-control text" id="date">

                        <input type="text" placeholder="Birthday"
                               value="<?= (isset($user['date'])) ? $user['date'] : '' ?>" id="birthday" name="dateEdit">
                        <button class="button"><span class="glyphicon glyphicon-calendar"></span></button>
                    </div>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="emailEdit" placeholder="Email *"
                           value="<?= (isset($user['email'])) ? $user['email'] : '' ?>" disabled class="form-control"
                           id="email-Edit">
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" name="addressEdit"
                           value="<?= (isset($user['address'])) ? $user['address'] : '' ?>" placeholder="Address"
                           class="form-control"
                           id="address-Edit">
                </div>
                <div class="form-group">
                    <button type="submit" name="btnUpdate" class="btn btn-primary"
                            style="float: right; border-radius: 0; width: 120px">Save
                    </button>
                </div>
            </div>
        </form>
        <div class="col-md-2"></div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 50%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body" style="height: 200px">
                <form action="#" method="POST">
                    <div class="form-group col-xs-12">
                        <label class="col-xs-2">Old Password</label>

                        <div class="col-xs-10">
                            <input type="password" name="old" required
                                   class="form-control" id="user-Edit">
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <label class="col-xs-2">New Password</label>

                        <div class="col-xs-10">
                            <input type="password" name="new" required
                                   class="form-control" id="user-Edit">
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <label class="col-xs-2">Confirm Password</label>

                        <div class="col-xs-10">
                            <input type="password" name="confirm" required
                                   class="form-control" id="user-Edit">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>

    </div>
</div>
<script>

    $(document).ready(function () {
        $(".btn-click").click(function () {
            $(".bar").toggle(500);
        });
    });

    $(function () {
        $("#date").datepicker();
    });

    function readURL(input, img_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + img_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar").change(function () {
        readURL(this, "blah");
    });
</script>