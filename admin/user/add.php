<?php
require("../models/User.php");
require("../layout/index.php");
$created_by = $_SESSION["idUser"];
?>


<title>Create Item</title>
<meta charset="UTF-8">
<link href="../../public/css/metro.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/user.css">
<script src="../../public/js/select2.min.js"></script>


<div class="content" style="">
    <div class="box-header">
        <a href="index.php" class="col-xs-6" style="margin-left: -15px"><h2 class="blue"><span
                    class="glyphicon glyphicon-chevron-left"></span>Employees &nbsp;</h2>
        </a>

        <div href="#" class="col-xs-6"><h2 class="blue">Add Employee &nbsp;</h2></div>


    </div>

    <div class="box-content" id="box-content" style="padding: 20px;">
        <div class="">
            <p class="introtext"><span style="margin-left: 40px">Please fill in the information below. The field labels marked with * are required input
                fields.</span></p>
        </div>
        <div class="col-md-2"></div>
        <form method="post" action="index.php" enctype="multipart/form-data" class="col-md-8">
            <div class="col-xs-5">
                <div class="form-group all">
                    <label id="img-avatar">
                        <input type="file" name="avatar" style="display: none;" id="avatar">
                        <img id="blah" class="img-default" src="../../upload/img/img-upload.png" title="avatar"
                             alt="your image" width="100%" height="100%"/>
                    </label>
                </div>
            </div>
            <div class="col-xs-7">
                <div class="form-group">
                    <input type="text" name="nameCreate" placeholder="Name *" required="required" class="form-control" id="name-create">
                </div>
                <div class="form-group">
                    <input type="text" name="userCreate" placeholder="UserName *" required="required" class="form-control" id="user-create">
                </div>
                <div class="form-group">
                    <input type="password" name="passCreate" placeholder="PassWord *" required="required" class="form-control"
                           id="pass-create">
                </div>
                <div class="input-control text" id="date">
                    <input type="text" placeholder="Birthday" id="birthday" name="dateCreate">
                    <button class="button"><span class="glyphicon glyphicon-calendar"></span></button>
                </div>
                <div class="form-group">
                    <input type="email" name="emailCreate" placeholder="Email *"  required="required" class="form-control" id="email-create">
                </div>
                <div class="form-group">
                    <input type="text" name="addressCreate" placeholder="Address" class="form-control"
                           id="address-create">
                </div>
                <div class="form-group">
                    <select class="form-control" name="roleCreate">
                        <option value="admin">Admin</option>
                        <option value="user">User</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" name="btnCreate" class="btn btn-primary"
                            style="float: right; border-radius: 0; width: 120px">Add Emplyee
                    </button>
                </div>
            </div>
        </form>
        <div class="col-md-2"></div>
    </div>
</div>

<script>

    $(document).ready(function () {
        $(".btn-click").click(function () {
            $(".bar").toggle(500);
        });
    });

    $(function () {
        $("#date").datepicker();
    });

    function readURL(input, img_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + img_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar").change(function () {
        readURL(this, "blah");
    });
</script>