<?php

require("../models/Layout.php");
if (isset($_POST["btnChange"])) {
    $id = $_SESSION["idUser"];
    $name = $_POST["nameChange"];
    $pass = md5($_POST["passChange"]);
    $date = $_POST["dateChange"];
    $email = $_POST["emailChange"];
    $add = $_POST["addressChange"];

    changeInfo($id, $name, $date, $email, $add, $pass);
}

$users = getInfoUserById($_SESSION["idUser"]);
?>

<head>
    <title>Home</title>
    <link rel="icon" href="../../upload/img/logo/logo1.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="../css/site.css">
</head>

<body style="overflow-x: hidden;">
<div class="body">
    <div class="body-header">
        <div class="btn-home"><a class="navbar-brand" href="#"><img src="../../upload/img/logo/logo.png"
                                                                    style="width: 100px; height:45px; margin-top: -12px"></a></div>
        <div class="btn-change-info">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a data-toggle="dropdown" href="#" style="color: white; margin-right: 40px">
                        <img src="<?= $users['url']?>" width="40px" height="40px" style="border-radius: 20px;">
                        <?= $users['name'] ?>&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu" style="width: 150px; margin-right: 40px;">
                        <li><a href="../user/edit_user.php"><span class="glyphicon glyphicon-pencil"></span> Change Profile</a></
                        <li class="divider"></li>
                        <li class="divider"></li>
                        <li style="margin-right: 30px; margin-top: 5px"><a href="../../logout.php"><span
                                    class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <div class="body-content container">
        <div class="row" style="margin-left: 10%">
            <div class="col-md-4 col-xs-4 item">
                <a href="../pos/index.php">
                    <img src="../../upload/img/layout/pos.png" width="150px" height="150px"><br>

                    <p style="text-align:center">POS</p>
                </a>
            </div>
            <div class="col-md-4 col-xs-4 item">
                <a href="../product/index.php">
                    <img src="../../upload/img/layout/item.png" width="150px" height="150px"><br>

                    <p style="text-align:center">Items</p>
                </a>
            </div>
            <div class="col-md-4 col-xs-4 item">
                <a href="../customer/index.php">
                    <img src="../../upload/img/layout/customer.png" width="150px" height="150px"><br>

                    <p style="text-align:center">Customers</p>
                </a>
            </div>
            <div class="col-md-4 col-xs-4 item">
                <a href="../report/report_form.php">
                    <img src="../../upload/img/layout/report.png" width="150px" height="150px"><br>

                    <p style="text-align:center">Reports</p>
                </a>
            </div>
            <div class="col-md-4 col-xs-4 item">
                <a href="../user/index.php">
                    <img src="../../upload/img/layout/employee.png" width="150px" height="150px"><br>

                    <p style="text-align:center">Employees</p>
                </a>
            </div>
            <div class="col-md-4 col-xs-4 item">
                <a href="../discount/index.php">
                    <img src="../../upload/img/layout/discount.png" width="150px" height="150px"><br>

                    <p style="text-align:center">Discounts</p>
                </a>
            </div>
            <div class="col-md-4 col-xs-4 item">
                <a href="../organization/index.php">
                    <img src="../../upload/img/layout/location.png" width="150px" height="150px"><br>

                    <p style="text-align:center">Location</p>
                </a>
            </div>
            <div class="col-md-4 col-xs-4 item">
                <a href="../setting/index.php">
                    <img src="../../upload/img/layout/setting.png" width="150px" height="150px"><br>

                    <p style="text-align:center">Setting</p>
                </a>
            </div>

        </div>
    </div>
</div>


<!-- Modal change -->
<div class="modal fade" id="modalChange" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color: #F2F4F7">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Change profile
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form class="form-horizontal" role="form" method="POST" method="#">
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-2 col-md-2 col-lg-2 control-label ">Name
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="nameChange" value="" class="form-control" id="name-change">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Birthday
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="date" name="dateChange" value="" class="form-control" id="date-change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Email
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="email" name="emailChange" value="" class="form-control" id="email-change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Address
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="type" name="addressChange" value="" class="form-control"
                                   id="address-change">
                        </div>
                    </div>
                    <br><br>

                    <h4 style="color: #489EE7">User Login Info</h4>
                    <hr>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-3 control-label ">Password
                            :</label>

                        <div class="col-sm-8 col-md-8 col-lg-9">
                            <input type="password" name="passChange" value="" class="form-control" id="pass-change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-3 control-label ">Confirm:</label>

                        <div class="col-sm-8 col-md-8 col-lg-9">
                            <input type="password" name="confirmPass" value="" class="form-control" id="pass-confirm">
                        </div>
                    </div>
                    <br><br>

                    <div class="input-modal">
                        <button type="submit" class="btn btn-primary" name="btnChange"
                                style="width: 100%; background-color: #489EE7">Submit
                        </button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
</body>
