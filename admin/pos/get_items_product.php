<?php
require ("../../config/db.php");

global $conn;


function getAllProductHaveDiscount(){
    global $conn;

    $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.pro_code, p.price, p.description, i.url, c.symbol, d.discount, d.start_date, d.end_date
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    JOIN discount d
                    ON p.pro_id = d.product_id
                    WHERE d.active = 1 AND i.avatar = 1");
    $result=[];
    if (!empty($sql)){
        while ($row = mysqli_fetch_assoc($sql)){
            $result[] = $row;
        }
    }
    return $result;
}

function getAllDiscountHaveTime(){
    $arr = getAllProductHaveDiscount();
    $now = new DateTime();
    $result = [];
    for($i=0; $i<count($arr); $i++){
        $start = new DateTime($arr[$i]["start_date"]);
        $end = new DateTime($arr[$i]["end_date"]);
        if($start <= $now && $end >= $now){
            $result[] = $arr[$i];
        }
    }
    return $result;
}

function getAllProductExist(){
    global $conn;
    $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.pro_code, p.price, p.description, i.url, c.symbol
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    WHERE p.active = 1");
    $result=[];
    if (!empty($sql)){
        while ($row = mysqli_fetch_assoc($sql)){
            $result[] = $row;
        }
    }
    return $result;
}


function getAll(){
    $a1 = getAllProductExist();
    $a2 = getAllDiscountHaveTime();
    return array_merge($a2, $a1);
}

echo json_encode(getAll());
?>