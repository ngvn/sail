<?php


function getAllCustomer()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT c.id, c.name, c.phone, c.address, i.url
                                FROM customer c
                                JOIN image_customer i
                                ON c.id = i.customer_id");
    $result = [];
    if ($sql) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    } else {
        $sql = mysqli_query($conn, "SELECT * FROM customer");
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}


function getCustomerrById($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM customer WHERE id = '$id'");
    $result = '';
    if ($sql) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result = $row;
        }
    }
    return $result;
}

function createCustomer($name, $phone, $address)
{
    global $conn;
    $name = addslashes($name);
    $phone = addslashes($phone);
    $address = addslashes($address);

    $sql = mysqli_query($conn, "INSERT INTO customer (name, phone, address) VALUES ('$name', '$phone', '$address')");
    return $sql;
}

function deleteCustomer($id)
{
    global $conn;
    $sql = mysqli_query($conn, "DELETE FROM customer WHERE id='$id'");
    return $sql;
}


function createImageCustomer($name, $customer_id, $url)
{
    global $conn;
    $name = addslashes($name);
    $customer_id = addslashes($customer_id);
    $url = addslashes($url);

    $sql = mysqli_query($conn, "INSERT INTO image_customer (name, customer_id, url) VALUES ('$name', '$customer_id', '$url')");
    return $sql;
}


function getAllImageCustomer()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM image_customer");
    $result = [];
    if ($sql) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}
