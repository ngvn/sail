<?php

function test_input($data)
{
    global $conn;
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}



function addCategory($name, $created_by)
{
    global $conn;
    $name = addslashes($name);
    $sql = mysqli_query($conn, "INSERT INTO category(cat_name, created_by) VALUES ('$name', '$created_by')");
    return $sql;
}

function getAllCategory()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM category");
    $result = [];
    if ($sql) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function addSubcategory($name, $category_id, $created_by)
{
    global $conn;
    $name = addslashes($name);
    $category_id = addslashes($category_id);

    $sql = mysqli_query($conn, "INSERT INTO subcategory(sub_name, category_id, created_by) VALUES ('$name', '$category_id', '$created_by')");
    return $sql;
}

function getAllSubcategory()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM subcategory");
    $result = [];
    if ($sql) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getNameCategoryById($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM category WHERE cat_id='$id'");
    $result = "";
    if ($sql) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result = $row;
        }
    }
    return $result["cat_name"];
}

function getNameSubcategory($id)
{
    global $conn;
    $result = "";
    $sql = mysqli_query($conn, "SELECT *
                        FROM subcategory
                        WHERE sub_id='$id'");
    if (!empty($sql)) {
        $row = mysqli_fetch_assoc($sql);
        $result = $row["sub_name"];
    }
    return $result;
}

function getProductCodeById($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM product WHERE pro_id='$id'");
    $result = "";
    if (!empty($sql)) {
        $row = mysqli_fetch_assoc($sql);
        $result = $row["pro_code"];
    }
    return $result;
}

function getNameCreated($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM user WHERE id='$id'");
    $result = "";
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result = $row["name"];
        }
    }
    return $result;
}

function getSubCategoryOfFirstCategory()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT MIN(cat_id) FROM category");
    $result2 = "";
    $cat_id ='';
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result2 = $row;
        }
    }
    if (isset($result2['MIN(cat_id)'])) {
        $cat_id = $result2['MIN(cat_id)'];
    }
    $sql = mysqli_query($conn, "SELECT sub_id, sub_name FROM subcategory WHERE category_id='$cat_id'");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getItemsOfFirstCategory()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT MIN(cat_id) FROM category");
    $result2 = "";
    $cat_id ='';
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result2 = $row;
        }
    }
    if (isset($result2['MIN(cat_id)'])) {
        $cat_id = $result2['MIN(cat_id)'];
    }
    $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.price, p.description, i.url, p.pro_code,
                          i.url, c.symbol, s.sub_id, s.sub_name, cat.cat_id, cat_name
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    JOIN subcategory s
                    ON p.subcategory_id = s.sub_id
                    JOIN category cat
                    ON s.category_id = cat.cat_id
                    WHERE p.active = 1 AND i.avatar = 1 AND cat.cat_id = '$cat_id'");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getCurrency()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM currency_item");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getItemById($id){
    global $conn;
    $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.price, p.description, i.img_id, i.url, p.pro_code,
                          i.url, c.symbol, s.sub_id, s.sub_name, cat.cat_id, cat_name
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    JOIN subcategory s
                    ON p.subcategory_id = s.sub_id
                    JOIN category cat
                    ON s.category_id = cat.cat_id
                    WHERE p.active = 1 AND p.pro_id = '$id'");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function addProduct($name, $code, $price, $subcategory_id, $description, $currency_id, $date_created, $active, $created_by)
{
    global $conn;
    $name = addslashes($name);
    $code = addslashes($code);
    $price = addslashes($price);
    $subcategory_id = addslashes($subcategory_id);
    $description = addslashes($description);
    $currency_id = addslashes($currency_id);
    $date_created = addslashes($date_created);
    $active = addslashes($active);
    $created_by = addslashes($created_by);

    $sql = mysqli_query($conn, "INSERT INTO product(pro_name, pro_code, price, subcategory_id, description, currency_id, date_created, active, created_by)
            VALUES ('$name', '$code', '$price', '$subcategory_id', '$description', '$currency_id', '$date_created', '$active', '$created_by')");
    return $sql;

}

function updateItem($id, $name, $code, $price, $subcategory_id, $description, $currency_id, $date_created, $active, $created_by)
{
    global $conn;
    $id = addslashes($id);
    $name = addslashes($name);
    $code = addslashes($code);
    $price = addslashes($price);
    $subcategory_id = addslashes($subcategory_id);
    $description = addslashes($description);
    $currency_id = addslashes($currency_id);
    $date_created = addslashes($date_created);
    $active = addslashes($active);
    $created_by = addslashes($created_by);

    $sql = mysqli_query($conn, "UPDATE product
                                SET pro_name='$name', pro_code='$code', price='$price', subcategory_id='$subcategory_id', description='$description',
                                currency_id='$currency_id', date_created='$date_created', active='$active', created_by='$created_by'
                                WHERE pro_id='$id'");

}

function getAllProducts()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM product");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getAllProductsOfCategory()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT p.id,p.name, p.price, p.description, p.code
                        i.url, s.name,
                    FROM product p
                    JOIN image_product i
                    ON product.id = image_product.product_id
                    JOIN subcategory s
                    ON p.subcategory_id = s.id
                    JOIN category c
                    ON s.category_id = c.id ");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getAllProductExist()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.price, p.description, i.url, p.pro_code,
                          i.url, c.symbol, s.sub_id, s.sub_name, cat.cat_id, cat_name
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    JOIN subcategory s
                    ON p.subcategory_id = s.sub_id
                    JOIN category cat
                    ON s.category_id = cat.cat_id
                    WHERE p.active = 1 AND i.avatar = 1");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}


function getSubCategoryByFirstCategory()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT MIN(cat_id) FROM category");
    $first = '';
    $cat_id = "";
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $first = $row;
        }
    }
    if ($first != '') {
        $cat_id = $first["MIN(cat_id)"];
    }
    $sql = mysqli_query($conn, "SELECT * FROM subcategory WHERE category_id = '$cat_id'");
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result = $row;
        }
    }
    return $result;
}

function getAll()
{
    $a1 = getAllProductExist();
    $a2 = getAllDiscountHaveTime();
    return array_merge($a1, $a2);
}


function getImageAllProducts()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.price, p.description, i.url, p.pro_code, i.url, c.symbol
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    WHERE p.active = 1");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

// image

function getAvatar($id, $avatar)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM image_product WHERE product_id='$id' AND avatar='$avatar'");
    $result = '../../upload/img/no_image.png';
    if (!empty($sql)) {
        $row = mysqli_fetch_assoc($sql);
        $result = $row["url"];
    }
    return $result;
}

function getAllImage($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM image_product WHERE product_id='$id'");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function updateImage($id, $name, $url){
    global $conn;
    $id = addslashes($id);
    $name = addslashes($name);
    $url = addslashes($url);

    $sql = mysqli_query($conn, "UPDATE image_product
                        SET img_name='$name', url='$url'
                        WHERE img_id='$id'");
    return $sql;
}

function deleteCategory($id)
{
    global $conn;
    $sql = mysqli_query($conn, "DELETE FROM category WHERE cat_id='$id'");
    return $sql;
}

function deleteProduct($id)
{
    global $conn;
    $sql = mysqli_query($conn, "DELETE FROM product WHERE pro_id='$id'");
    return $sql;
}

// Discount

function getAllProductHaveDiscount()
{
    global $conn;

    $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.price, p.description, i.url, p.pro_code,
                          i.avatar, d.discount, d.active, d.start_date, d.end_date, c.symbol
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    JOIN discount d
                    ON p.pro_id = d.product_id
                    WHERE d.active = 1 AND i.avatar = 1");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    } else {
        $sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.price, p.description, p.pro_code, d.discount, c.symbol
                        FROM product p
                        JOIN currency_item c
                        ON p.currency_id = c.id
                        JOIN discount d
                        ON p.pro_id = d.product_id
                        WHERE d.active = 1");
        if (!empty($sql)) {
            while ($row = mysqli_fetch_assoc($sql)) {
                $result[] = $row;
            }
        }
    }

    return $result;
}


function getAllDiscountHaveTime()
{
    global $conn;
    $arr = getAllProductHaveDiscount();
    $now = new DateTime();
    $result = [];
    for ($i = 0; $i < count($arr); $i++) {
        $start = new DateTime($arr[$i]["start_date"]);
        $end = new DateTime($arr[$i]["end_date"]);
        if ($start <= $now && $end >= $now) {
            $result[] = $arr[$i];
        }
    }
    return $result;
}


function getDiscountProduct($id)
{
    global $conn;
    $result = 0;
    $sql = mysqli_query($conn, "SELECT * FROM discount WHERE product_id='$id' AND avatar='$id'");
    if (!empty($sql)) {
        $row = mysqli_fetch_assoc($sql);
        $result = $row["discount"];
    }
    return $result;
}

function deleteDiscountByProductId($id){
    global $conn;
    $sql = mysqli_query($conn,"DELETE FROM discount WHERE product_id='$id'");
    return $sql;
}