<?php

function getAllUser()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM user");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getUserById($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM user WHERE id = '$id'");
    $result = '';
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result = $row;
        }
    }
    return $result;
}

function editUser()
{

}

function getAllOrganization()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM organization");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function getOrganization($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM organization WHERE org_id='$id'");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function createUser($name, $user, $pass, $date, $address, $email, $role, $org)
{
    global $conn;
    $name = addslashes($name);
    $user = addslashes($user);
    $pass = addslashes($pass);
    $address = addslashes($address);
    $email = addslashes($email);
    $role = addslashes($role);
    $org = addslashes($org);

    $sql = mysqli_query($conn, "INSERT INTO user(name, user_name, pass_word, date, address, email, role, organization_id) VALUES
                        ('$name', '$user', '$pass', '$date', '$address', '$email', '$role', '$org')");
    return $sql;
}

function deleteUser($id)
{
    global $conn;
    $sql = mysqli_query($conn, "DELETE FROM user WHERE id='$id'");
    return $sql;
}

function deleteOrderByUserId($id)
{
    global $conn;
    $sql = mysqli_query($conn, "DELETE FROM item_order WHERE salesman_id='$id'");
    return $sql;
}

function createAvatar($name, $employee_id, $url)
{
    global $conn;

    $sql = mysqli_query($conn, "INSERT INTO image_employee (name, employee_id, url) VALUES ('$name', '$employee_id', '$url');");
    return $sql;
}

function deleteAvatar($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM image_employee WHERE employee_id='$id'");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result = $row;
        }
    }
    if ($result['employee_id'] == $id) {
        if($result['url']!="../../upload/employee/avatar-default.jpg"){
            unlink($result['url']);
        }
        mysqli_query($conn, "DELETE FROM image_employee WHERE employee_id='$id'");
    }

    return $sql;
}

function getInfoUser()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT u.id, u.name, u.email, u.user_name, u.role, i.url
                                FROM user u
                                JOIN image_employee i
                                ON u.id=i.employee_id ");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    if (empty($result)) {
        $sql = mysqli_query($conn, "SELECT * FROM user");
        if (!empty($sql)) {
            while ($row = mysqli_fetch_assoc($sql)) {
                $result[] = $row;
            }
        }
    }
    return $result;
}

function updateUser($id, $name, $address, $birthday){
    global $conn;
    $sql = mysqli_query($conn, "UPDATE user SET name='$name', address='$address', date='$birthday' WHERE id='$id'");
    return $sql;
}

