<?php

function countUser()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM user");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_array($sql)) {
            $result[] = $row;
        }
    }
    return count($result);
}

function countProduct()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM product");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_array($sql)) {
            $result[] = $row;
        }
    }
    return count($result);
}

function countDiscount()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM discount");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_array($sql)) {
            $result[] = $row;
        }
    }
    return count($result);
}



function getInfoUserById($id)
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT u.id, u.name, u.email, u.user_name, u.role, i.url
                                FROM user u
                                JOIN image_employee i
                                ON u.id=i.employee_id
                                WHERE u.id='$id'");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result = $row;
        }
    }
    return $result;
}

