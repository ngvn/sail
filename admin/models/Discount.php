<?php



function getAllDiscount()
{
    global $conn;
    $sql = mysqli_query($conn,"SELECT * FROM discount");
    $result = [];
    if ($sql) {
        while ($row = mysqli_fetch_array($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}


function getNameProductById($id){
    global $conn;
    $id = addslashes($id);
    $result = "";
    $sql = mysqli_query($conn,"SELECT * FROM product WHERE dis_id='$id'");
    if(!empty($sql)){
        $row  = mysqli_fetch_assoc($sql);
        $result = $row["dis_name"];
    }
    return $result;
}

function getDiscountrById($id)
{
    global $conn;
    $sql = mysqli_query($conn,"SELECT * FROM discount WHERE dis_id = '$id'");
    $result = '';
    if ($sql) {
        while ($row = mysqli_fetch_array($sql)) {
            $result = $row;
        }
    }
    return $result;
}

function createDiscount($name, $product_id, $start, $end, $discount, $active, $created_by)
{
    global $conn;
    $name = addslashes($name);
    $product_id = addslashes($product_id);
    $start = addslashes($start);
    $end = addslashes($end);
    $discount = addslashes($discount);
    $created_by = addslashes($created_by);

    $sql = mysqli_query($conn,"INSERT INTO discount(discount_program_name, product_id, start_date, end_date, discount, active, created_by)
                         VALUES ('$name', '$product_id', '$start', '$end', '$discount', '$active','$created_by')");
    return $sql;
}

function deleteDiscount($id){
    global $conn;
    $sql = mysqli_query($conn,"DELETE FROM discount WHERE dis_id='$id'");
    return $sql;
}