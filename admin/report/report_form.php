<?php
require("../models/Report.php");
require("../layout/index.php");


$employee = getAllEmployee();
?>

<meta charset="UTF-8">
<title>Report</title>

<script src="../../public/js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/report.css">

<div class="content">

    <div class="box-header" style="margin-bottom: 10px;">
        <div class="col-md-6 col-sm-6">
            <a href="../site/index.php" style="margin-left: -15px">
                <h2 class="blue">
                    <span class="glyphicon glyphicon-chevron-left"></span>Home &nbsp;
                </h2>
            </a>
        </div>
        <div class="col-md-5 col-sm-5"><h2 class="blue">Report &nbsp;</h2></div>
        <div class="col-md-1 col-sm-1"></div>
    </div>
    <div class="box-content">

        <form class="col-md-12 col-lg-12 col-sm-12" action="index.php" method="post">

            <div class="form-group">
                <label for="complex_radio" class="col-sm-3 col-md-3 col-lg-2 control-label">Custom Range
                    :</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <div class="input-control text" id="from">
                        <input type="text" placeholder="From Date" id="from-date" name="fromDate">
                        <button class="button"><span class="glyphicon glyphicon-calendar"></span></button>
                    </div>

                    <div class="input-control text" id="to">
                        <input type="text" placeholder="To Date" id="to-date" name="toDate">
                        <button class="button"><span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="s2id_autogen1" class="col-sm-3 col-md-3 col-lg-2 control-label ">Employee :</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select class="employee-select" id="client-select" name="employeeId" style="width: 40%">
                        <option value="0"> All</option>
                        <?php for($i=0 ;$i<count($employee); $i++){?>
                        <option value="<?= $employee[$i]['id']?>" > <?= $employee[$i]['name']?></option>
                        <?php }?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="s2id_autogen1" class="col-sm-3 col-md-3 col-lg-2 control-label ">Locations :</label>

                <div class="col-sm-9 col-md-9 col-lg-10">
                    <select class="employee-select" id="client-select" name="clientId" style="width: 40%">
                        <option value="0"> All</option>
                        <option value="1"> Main</option>
                        <option value="2"> Organization1</option>
                        <option value="3"> Store1</option>
                        <option value="4"> Store2</option>
                    </select>
                </div>
            </div><br><br>

            <div class="form-group col-md-12" style="margin-top: 20px">
                <label for="s2id_autogen1" class="col-sm-3 col-md-3 col-lg-2 control-label "></label>

                <button type="submit" id="btn_report" name="btnReport"
                        style="border-radius: 0; width: 200px; margin: 5px" class="btn btn-primary">
                    Submit
                </button>
            </div>
        </form>
    </div>
</div>


<script>
    $(function () {
        $("#from").datepicker();
        $("#to").datepicker();
    });
    $(document).ready(function () {
        $(".employee-select").select2({
            placeholder: "Select a category",
            allowClear: true
        });
    });
</script>