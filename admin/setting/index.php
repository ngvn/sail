<?php

require("../layout/index.php");
require("../models/Setting.php");

if (isset($_POST['btnChangeCurrency'])) {
    $id = 1;
    $name = isset($_POST["currencyName"]) ? $_POST["currencyName"] : "GBP";
    if ($name == "USD") $symbol = "$";
    else if ($name == "VNĐ") $symbol = "₫";
    else if ($name == "JPY") $symbol = "¥";
    else $symbol = "£";
    updateCurrency($id, $name, $symbol);
}

$symbol = getSymbol();
?>


<meta charset="UTF-8">
<title>Setting</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/setting.css">


<div class="content">

    <div class="box-header" style="margin-bottom: 10px;">
        <a href="../site/index.php" class="col-lg-6 col-md-6 col-sm-6" style="margin-left: -15px"><h2
                class="blue"><span
                    class="glyphicon glyphicon-chevron-left"></span>Home &nbsp;</h2>
        </a>

        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-left: 0px;"><h2 class="blue">Settings &nbsp;</h2>
        </div>
        <!--
        <div class="col-lg-1 col-md-1 col-sm-1">
            <h2 class="blue" style="float: right!important; margin-right: -15px;">
                        <span data-toggle="tooltip" title="Export" data-placement="left"
                              class="glyphicon glyphicon-export"></span>
            </h2>
        </div>
        -->
    </div>
    <div class="box-content">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#employee"><span class="glyphicon glyphicon-user"></span>
                    Employees</a></li>
            <li><a data-toggle="tab" href="#company"><span class="glyphicon glyphicon-cog"></span> Company</a>
            </li>
            <li><a data-toggle="tab" href="#sale"><span class="glyphicon glyphicon-barcode"></span> Sales</a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="employee" class="tab-pane fade in active">
                <h3>Employees</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua.</p>
            </div>
            <div id="company" class="tab-pane fade">
                <h3>Menu 1</h3>

                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.</p>
            </div>
            <div id="sale" class="tab-pane fade">
                <h3>Sales Setting</h3>

                <form action="#" method="post">
                    <label class="col-xs-3 first-label">Setting currency</label>

                    <div>
                        <label class="radio-inline">
                            <input type="radio" name="currencyName"
                                   value="GBP" <?= $symbol[0]['name'] == "GBP" ? "checked" : "" ?> /><i>GBP £</i>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="currencyName"
                                   value="VNĐ" <?= $symbol[0]['name'] == "VNĐ" ? "checked" : "" ?> /><i>VNĐ ₫</i>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="currencyName"
                                   value="USD" <?= $symbol[0]['name'] == "USD" ? "checked" : "" ?> /><i>USD $</i>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="currencyName"
                                   value="JPY" <?= $symbol[0]['name'] == "JPY" ? "checked" : "" ?> /><i>JPY ¥</i>
                        </label>
                    </div>
                    <label class="col-xs-12 btn-submit">
                        <button class="btn btn-primary" name="btnChangeCurrency">Save</button>
                    </label>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
