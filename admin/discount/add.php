<?php
require("../layout/index.php");
require("../models/Discount.php");
require("../models/Product.php");

$products = getAllProducts();
?>

<title>Create Discount</title>
<meta charset="UTF-8">
<link href="../../public/css/metro.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/discount.css">
<script src="../../public/js/select2.min.js"></script>
<script>

    $(function () {
        $(".product-select").select2({
            placeholder: "Select a product code",
            allowClear: true
        });
        $(".active-select").select2({
            placeholder: "Active",
            allowClear: true
        });
    });
</script>


<div class="content">
    <div class="box-header col-xs-12">
        <div class=" col-xs-6">
            <a href="index.php">
                <h2 class="blue" style=" margin-left: -20px">
                    <span class=" glyphicon glyphicon-chevron-left"></span>Discounts &nbsp;</h2>
            </a>
        </div>
        <div class="col-xs-6"><h2 class="blue">Add Discounts &nbsp;</h2></div>

    </div>
    <div class="box-content">
        <div class="">
            <p class="introtext" style="margin: 20px -20px 20px -20px;">Please fill in the information below. The field
                labels marked with * are required input
                fields.</p>
        </div>
        <div class="col-xs-2"></div>
        <form action="index.php" method="post" class="col-xs-8 col-sm-8">
            <div class="form-group">
                <label>Promotions *</label>
                <input type="text" name="discountPromotions" value="" class="form-control" id="cate-name"
                       required="required"
                       data-bv-field="promotions">
            </div>
            <div class="form-group">
                <label for="category">Product Code*</label>
                <select class="product-select full-size" name="productId" style="width: 100%" required>
                    <?php for ($i = 0; $i < count($products); $i++) { ?>

                        <option value="<?= $products[$i]['pro_id'] ?>"> <?= $products[$i]['pro_code'] ?></option>

                    <?php } ?>
                </select>
            </div>
            <div class="form-group">

                <div class="input-control text" id="from" style="padding-left: 0; padding-right: 0">
                    <input type="text" placeholder="From Date" id="start" name="start" required="required">
                    <button class="button"><span class="glyphicon glyphicon-calendar"></span></button>
                </div>

                <div class="input-control text" id="to" style="padding-left: 0; padding-right: 0">
                    <input type="text" placeholder="To Date" id="end" name="end" required="required">
                    <button class="button"><span class="glyphicon glyphicon-calendar"></span>
                    </button>
                </div>
            </div>

            <div class="form-group">
                <label>Discount *</label>
                <input type="number" name="discount" min="0" max="99" class="form-control" id="discount"
                       required="required"
                       data-bv-field="promotions">
            </div>
            <div class="form-group">
                <label>Active *</label>
                <select class="active-select full-size" name="active" style="width: 100%">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" name="discountAdd"
                        style="border-radius: 0" class="btn btn-primary">Add Discount
                </button>

            </div>
        </form>
        <div class="col-xs-2"></div>
    </div>
</div>

<script>
    $(function () {
        $("#from").datepicker();
        $("#to").datepicker();
    });
</script>



