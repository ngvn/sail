<?php
require("../models/Discount.php");
require("../models/Product.php");
require("../layout/index.php");

if (isset($_POST["discountAdd"])) {
    $name = $_POST["discountPromotions"];
    $product_id = $_POST["productId"];
    $start = str_replace('.', '-', $_POST["start"]);
    $end = str_replace('.', '-', $_POST["end"]);
    $discount = $_POST["discount"];
    $active = $_POST["active"];

    $created_by = $_SESSION["idUser"];

    createDiscount($name, $product_id, $start, $end, $discount, $active, $created_by);
}

$discounts = getAllDiscount();

if(isset($_POST["btnDelete"])){
    $id=$_POST["id"];
    deleteDiscount($id);
}
?>

<meta charset="UTF-8">
<title>Discounts</title>

<script src="../../public/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/discount.css">


<div class="content">
    <div class="box-header col-md-12 col-sm-12">
        <div class="col-md-6 col-sm-6">
            <a href="../site/index.php">
                <h2 class="blue" style=" margin-left: -20px">
                    <span class=" glyphicon glyphicon-chevron-left"></span>Home &nbsp;</h2>
            </a>
        </div>
        <div class="col-md-5 col-sm-6"><h2 class="blue">Discounts &nbsp;</h2></div>
        <div class="col-md-1 col-sm-1"><a href="add.php"><h2 class="blue" style=" margin-left: 80px">
                <span data-toggle="tooltip" title="Add" data-placement="left"
                      class="glyphicon glyphicon-plus"></span>
            </h2></a></div>
    </div>
    <div class="box-content">
        <table class="table table-bordered" id="list-discounts"  style="font-size: small">
            <thead>
            <tr>
                <th style="text-align: center">Promotions</th>
                <th style="text-align: center">Product Code</th>
                <th style="text-align: center">Start</th>
                <th style="text-align: center">End</th>
                <th style="text-align: center">Discount</th>
                <th style="text-align: center">Created By</th>
                <th style="text-align: center">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < count($discounts); $i++) { ?>
                <tr>
                    <td><?php echo $discounts[$i]['discount_program_name']; ?></td>
                    <td style="text-align: center"><?php echo getProductCodeById($discounts[$i]['product_id']); ?></td>
                    <td style="text-align: center"><?php echo $discounts[$i]['start_date']; ?></td>
                    <td style="text-align: center"><?php echo $discounts[$i]['end_date']; ?></td>
                    <td style="text-align: center"><?php echo $discounts[$i]['discount']; ?></td>
                    <td style="text-align: center"><?php echo getNameCreated($discounts[$i]['created_by']); ?></td>
                    <td style="text-align: center">
                        <form method="post" action="#" style="display: inline">
                            <input type="hidden" name="id" value="<?= $discounts[$i]['dis_id']; ?>">

                            <button class="btn btn-danger" name="btnDelete" type="submit" style="font-size: x-small"
                                    value="delete"
                                    onclick="return confirm('Are you sure to delete?');">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>

    $(document).ready(function () {

        $('#list-discounts').DataTable();
        $(".popover-menu").click(function () {
            $(".menu-content").toggle();
        });
    });
</script>