<?php
require("../models/Product.php");
require("../models/Image_product.php");
require("../layout/index.php");
include('../../public/class/SimpleImage.php');
global $conn;

if (isset($_POST["addProduct"])) {
    $name = $_POST["nameProduct"];
    $code = $_POST["codeProduct"];
    $price = $_POST["priceProduct"];
    $subcategory_id = $_POST["subcategoryProduct"];
    $description = $_POST["descriptionProduct"];
    $currency_id = 1;
    $date_created = date("Y-m-d");
    $active = 1;
    $created_by = $_SESSION["idUser"];
    addProduct($name, $code, $price, $subcategory_id, $description, $currency_id, $date_created, $active, $created_by);
    $product_id = mysqli_insert_id($conn);
    if (!empty($_FILES['avatar']['name'])) {
        $avatar = $_FILES['avatar'];
        $file_loc = $_FILES['avatar']['tmp_name'];
        $file = $code . '.jpg'; //tên ảnh
        $url = "../../upload/product/" . $file;
        $type = 1;
        move_uploaded_file($file_loc, $url);
        if($_FILES['avatar']['size'] > 102400) {
            $image = new SimpleImage();
            $image->load($url);
            $image->resize(250, 400);
            $image->save($url);
        }
        createImage($file, $url, 1, $product_id);
    }

    for ($i = 1; $i < 7; $i++) {
        if (!empty($_FILES['img' . $i]['name'])) {
            $avatar = $_FILES['img' . $i];
            $file_loc = $_FILES['img' . $i]['tmp_name'];
            $file = $code. $i . '.jpg'; //tên ảnh
            $url = "../../upload/product/" . $file;
            move_uploaded_file($file_loc, $url);
            if($_FILES['img' . $i]['size'] > 102400){
                $image = new SimpleImage();
                $image->load($url);
                $image->resize(250, 400);
                $image->save($url);
            }
            createImage($file, $url, 0, $product_id);
        }
    }
}

if (isset($_POST["saveItem"])) {
    $id = $_POST["idItem"];
    $name = $_POST["nameItem"];
    $code = $_POST["codeItem"];
    $price = $_POST["priceItem"];
    $subcategory_id = $_POST["subcategoryItem"];
    $description = $_POST["descriptionItem"];
    $currency_id = 1;
    $date_created = date("Y-m-d");
    $active = 1;
    $created_by = $_SESSION["idUser"];
    updateItem($id, $name, $code, $price, $subcategory_id, $description, $currency_id, $date_created, $active, $created_by);


    if (!empty($_FILES['avatarItem']['name'])) {

        $avatar_id = $_POST["IdAvatarItem"];
        $file_loc = $_FILES['avatarItem']['tmp_name'];
        $file = $code . '.jpg'; //tên ảnh
        $url = "../../upload/product/" . $file;
        $type = 1;
        move_uploaded_file($file_loc, $url);
        if($_FILES['avatarItem']['size'] > 102400) {
            $image = new SimpleImage();
            $image->load($url);
            $image->resize(250, 400);
            $image->save($url);
        }
        updateImage($avatar_id, $file, $url, 1);
    }

    for ($i = 1; $i < 7; $i++) {
        if(!empty($_POST["IdImg".$i."Item"])){
            $img_id = $_POST["IdImg".$i."Item"];
            $file = $code. $i . '.jpg'; //tên ảnh
            $url = "../../upload/product/" . $file;
            $old_img = getImageById($img_id);
            rename($old_img['url'],$url);
            updateImage($img_id, $file, $url);
        }
        if (!empty($_FILES['img' . $i .'Item']['name'])) {
            $img_id = $_POST["IdImg".$i."Item"];

            $file_loc = $_FILES['img' . $i .'Item']['tmp_name'];
            $file = $code. $i . '.jpg'; //tên ảnh
            $url = "../../upload/product/" . $file;
            move_uploaded_file($file_loc, $url);
            if(empty($_POST["IdImg".$i."Item"])){
                createImage($file, $url, 0, $id);
            }
            else{
                updateImage($img_id, $file, $url);
            }
            if($_FILES['img' . $i .'Item']['size'] > 102400){
                $image = new SimpleImage();
                $image->load($url);
                $image->resize(250, 400);
                $image->save($url);
            }
        }
    }
}
// Xoa product + imageProduct
if (isset($_POST["btnProductDelete"])) {
    $id = $_POST['productId'];
    deleteProduct($id);
    deleteDiscountByProductId($id);
    $image_delete = getAllImage($id);
    for ($i = 0; $i < count($image_delete); $i++) {

        if (isset($image_delete[$i]["url"])) {
            unlink($image_delete[$i]["url"]);
        }
        deleteImage($image_delete[$i]["img_id"]);
    }
}

$sub = getAllSubcategory();
$products = getAllProducts();
?>


<meta charset="UTF-8">
<title>Products</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">
<script type="text/javascript" src="../../public/js/jquery.dataTables.min.js"></script>
<script src="../../public/js/owl.carousel.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../public/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="../../public/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="../css/product.css">


<div class="content">
    <div class="product-list">


        <div id="product">
            <div class="box-header" style="margin-bottom: 20px;">
                <a href="../site/index.php" class="col-md-6" style="margin-left: -15px"><h2 class="blue"><span
                            class="glyphicon glyphicon-chevron-left"></span>Home &nbsp;</h2>
                </a>

                <div class="col-md-5"><h2 class="blue">Items &nbsp;</h2></div>
                <div class="col-md-1">
                    <a href="#"><h2 class="blue" id="btn-zoom"><span data-toggle="tooltip" title="Search" data-placement="right"
                                class="glyphicon glyphicon-zoom-in"></span></h2></a>
                    <a href="add_product.php"><h2 class="blue"><span data-toggle="tooltip" title="Add Item" data-placement="left"
                                class="glyphicon glyphicon-plus"></span></h2></a>
                </div>


            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered" id="list-products">
                    <thead>
                    <tr>
                        <th style="text-align: center">Image</th>
                        <th style="text-align: center">Name</th>
                        <th style="text-align: center">Code</th>
                        <th style="text-align: center">Category</th>
                        <th style="text-align: center">Price</th>
                        <th style="text-align: center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < count($products); $i++) { ?>
                        <tr ondblclick="$('#edit-item-form-<?= $products[$i]['pro_id'] ?>').submit()" style="cursor: pointer">
                            <td style="text-align: center; width: 65px"><img
                                    src="<?= getAvatar($products[$i]['pro_id'], 1) ?>"
                                    style="width: 35px; height: 35px;"></td>
                            <td><?php echo $products[$i]['pro_name']; ?></td>
                            <td><?php echo $products[$i]['pro_code']; ?></td>
                            <td><?php echo getNameSubcategory($products[$i]['subcategory_id']) ?></td>
                            <td><?php echo number_format($products[$i]['price']); ?></td>
                            <td style="width: 105px;">
                                <form method="post" action="edit_item.php?id=<?= $products[$i]['pro_id'] ?>" id="edit-item-form-<?= $products[$i]['pro_id'] ?>">
                                </form>
                                <form method="post" action="#" id="delete-product-form" style="display: inline">
                                    <input type="hidden" name="productId" value="<?= $products[$i]['pro_id']; ?>">

                                    <a href="#" class="btn btn-info" style="font-size: x-small"
                                       onclick="viewProductInfo(<?= $products[$i]['pro_id'] ?>)" name="productInfo"
                                       data-toggle="modal" data-target="#productInfo">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>

                                    <button class="btn btn-danger" name="btnProductDelete" type="submit"
                                            onclick="return confirm('Are you sure to delete?');">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div id="modal"></div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });

        $('#list-products').DataTable();
        $(".popover-menu").click(function () {
            $(".menu-content").toggle();
        });

        $("#btn-zoom").click(function(){
            $("label").toggle();
        });

        $('[data-toggle="tooltip"]').tooltip();
    });

    //modal
    function viewProductInfo(value) {

        $(document).ready(function () {
            $.ajax({
                url: "get_all_image.php",
                type: "post",
                dataType: "json",
                data: {"id": value},
                async: false,
                success: function (data) {
                    console.log(data);
                    var content = '';
                    var header = '<div class="modal fade" id="productInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
                        '<div class="modal-dialog" style="width: 70%">' +
                        '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal">' +
                        '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                        '<h4 class="modal-title" id="myModalLabel">' + data[0]["pro_name"] + '</h4></div>' +
                        '<div class="modal-body">' +
                        '<div id="owl-image">';
                    if (data[0]['url']) {
                        for (var i = 0; i < data.length; i++) {

                            content = content + '<div class="item"><img src="' + data[i]['url'] + '" style ="width: 300px; height: 300px"></div>';
                        }
                    }
                    else {
                        content = '<div class="item"><img src="../../upload/img/no_image.png" style ="width: 300px; height: 300px"></div>';
                    }
                    var footer = '</div><br>' + '<div><b>Price: </b>' + data[0]['price'].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' <b>' + data[0]['symbol'] + '</b><br><br>' +
                        '<b>Code: </b>' + data[0]['pro_code'] + '<br><br>' +
                        '<b>Description: </b>' + data[0]['description'] + '<br><br>' + '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default" data-dismiss="modal">' +
                        'Close</button></div></div></div></div>';

                    var modal = header + content + footer;

                    document.getElementById("modal").innerHTML = modal;

                    $("#owl-image").owlCarousel({

                        autoPlay: 3000, //Set AutoPlay to 3 seconds
                        items: 3,
                        itemsDesktop: [1199, 3],
                        itemsDesktopSmall: [979, 3]
                    });
                }
            });
        });
    }
</script>
