<?php
require("../../config/db.php");

global $conn;
$id = isset($_POST["id"])? $_POST["id"]: "";
$sql = mysqli_query($conn, "SELECT p.pro_name, p.pro_code, p.price, i.url,
                    FROM product p
                    JOIN currency c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    WHERE active = 1 AND pro_id='$id'");
$result = [];
if (!empty($sql)) {
    while ($row = mysqli_fetch_assoc($sql)) {
        $result[] = $row;
    }
}

echo json_encode($result);
?>