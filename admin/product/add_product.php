<?php
require("../models/Product.php");
require("../models/Image_product.php");
require("../layout/index.php");
$created_by = $_SESSION["idUser"];

if (isset($_POST["categoryAdd"])) {
    $name = test_input($_POST["catName"]);
    addCategory($name, $created_by);
}

if (isset($_POST["subcategoryAdd"])) {
    $name = test_input($_POST["subName"]);
    $category_id = test_input($_POST["categoryId"]);
    addSubcategory($name, $category_id, $created_by);
}

$categories = getAllCategory();
$sub = getAllSubcategory();
?>


<title>Create Item</title>
<meta charset="UTF-8">
<link href="../../public/css/metro.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/product.css">
<script src="../../public/js/select2.min.js"></script>


<div class="content" style="">
    <div class="box-header">
        <a href="index.php" class="col-md-6" style="margin-left: -15px"><h2 class="blue"><span
                    class="glyphicon glyphicon-chevron-left"></span>Items &nbsp;</h2>
        </a>

        <div href="#" class="col-md-5"><h2 class="blue">Add Item &nbsp;</h2></div>
        <a href="add_product.php" class="col-md-1" style=""><h2 class="blue">
                <span class=""></span></h2>
        </a>


    </div>

    <div class="box-content" id="box-content" style="padding: 20px;">
        <div class="">
            <p class="introtext"><span style="margin-left: 40px">Please fill in the information below. The field labels marked with * are required input
                fields.</span></p>
        </div>

        <form method="post" action="index.php" enctype="multipart/form-data" class="col-md-12">

            <div class="col-md-7">
                <div class="form-group col-md-12">
                    <label for="name">Product Name *</label>
                    <input type="text" name="nameProduct" class="form-control" id="prd-name"
                           required="required"
                           data-bv-field="name">
                </div>
                <div class="form-group all col-md-12">
                    <div class="col-md-6" style="padding-left: 0; padding-right: 0">
                        <label for="code">Product Code *</label>
                        <input type="text" name="codeProduct" class="form-control" id="prd-code"
                               required="required"
                               data-bv-field="code">
                    </div>
                    <div class="col-md-6" style="padding-left: 0; padding-right: 0">
                        <label for="price">Product Price *</label>
                        <input type="text" name="priceProduct" class="form-control" id="prd-price"
                               required="required"
                               data-bv-field="price">
                    </div>
                </div>
                <br>

                <div class="form-group all col-xs-12">

                    <div class="col-xs-5" style="padding-left: 0; padding-right: 0">
                        <label for="category">Category *</label>
                        <select class="category-select form-control" name="categoryProduct"
                                style="width: 100%">
                            <?php for ($i = 0; $i < count($categories); $i++) { ?>

                                <option
                                    value="<?= $categories[$i]['cat_id'] ?>"> <?= $categories[$i]['cat_name'] ?></option>

                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-xs-1" style="padding-left: 0; padding-right: 0; ">
                        <label>Add</label>
                        <a href="#" data-toggle="modal" data-target="#categoryModal"
                           style=" display: block; border: 1px solid #388fe8; width: 100%; height: 34px; margin-top: 5px; padding: 8px 8px 8px 22px;
                                    background-color: #388fe8;color: white;">
                            <span class="glyphicon glyphicon-plus" data-toggle="tooltip"
                                  title="Add Category!">
                            </span>
                        </a>
                    </div>
                    <div class="col-xs-5" style="padding-left: 0; padding-right: 0">
                        <label for="category">SubCategory *</label>
                        <select class="subcategory-select form-control" id="sub-select" name="subcategoryProduct"
                                style="width: 100%; margin-top: 5px">
                            <?php for ($i = 0; $i < count($sub); $i++) { ?>

                                <option value="<?= $sub[$i]['sub_id'] ?>"> <?= $sub[$i]['sub_name'] ?></option>

                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-xs-1" style="padding-left: 0; padding-right: 0; ">
                        <label>Add</label>
                        <a href="#" data-toggle="modal" data-target="#subcategoryModal"
                           style=" display: block; border: 1px solid #388fe8; width: 100%; height: 34px; margin-top: 5px; padding: 8px 8px 8px 22px;
                                    background-color: #388fe8;color: white;">
                            <span class="glyphicon glyphicon-plus" data-toggle="tooltip"
                                  title="Add SubCategory!">
                            </span>
                        </a>
                    </div>
                </div>
                <br>

                <div class="form-group all col-md-12">
                    <label for="price">Description</label>
                    <textarea class="form-control" name="descriptionProduct"></textarea>
                </div>
            </div>

            <div class="col-md-5">
                <div class="form-group all">
                    <label for="price">List Images</label>

                    <div class="upload-image">
                        <div class="col-xs-4" style="padding: 1px;">
                            <label style="width: 220px; height: 270px;">
                                <input type="file" name="avatar" style="display: none;" id="avatar">
                                <img id="blah" class="img-default" src="../../upload/img/img-upload.png"
                                     alt="your image" width="100%" height="100%"/>
                            </label>
                        </div>
                        <div class="col-xs-8">
                            <label>
                                <input type="file" name="img1" style="display: none;" id="img-1">
                                <img id="blah1" class="img-default" src="../../upload/img/img-upload.png"
                                     alt="your image"
                                     width="100%" height="100%"/>
                            </label>
                            <label>
                                <input type="file" name="img2" style="display: none;" id="img-2">
                                <img id="blah2" class="img-default" src="../../upload/img/img-upload.png"
                                     alt="your image"
                                     width="100%" height="100%"/>
                            </label>
                            <label>
                                <input type="file" name="img3" style="display: none;" id="img-3">
                                <img id="blah3" class="img-default" src="../../upload/img/img-upload.png"
                                     alt="your image"
                                     width="100%" height="100%"/>
                            </label>
                            <label>
                                <input type="file" name="img4" style="display: none;" id="img-4">
                                <img id="blah4" class="img-default" src="../../upload/img/img-upload.png"
                                     alt="your image"
                                     width="100%" height="100%"/>
                            </label>
                            <label>
                                <input type="file" name="img5" style="display: none;" id="img-5">
                                <img id="blah5" class="img-default" src="../../upload/img/img-upload.png"
                                     alt="your image"
                                     width="100%" height="100%"/>
                            </label>
                            <label>
                                <input type="file" name="img6" style="display: none;" id="img-6">
                                <img id="blah6" class="img-default" src="../../upload/img/img-upload.png"
                                     alt="your image"
                                     width="100%" height="100%"/>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <button type="submit" name="addProduct" class="btn btn-primary"
                        style="margin: 13px; border-radius: 0; width: 120px">Add Product
                </button>
                <button type="reset" class="btn btn-danger"
                        style="border-radius: 0">Cancel
                </button>
            </div>
        </form>
    </div>
</div>

<!-- Modal add category-->
<div class="modal fade" id="categoryModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Category</h4>
            </div>
            <div class="modal-body" style="height: 100px">
                <form action="#" method="post" enctype="multipart/form-data" class="col-md-10">
                    <div class="form-group">
                        <label for="name">Category Name *</label>
                        <input type="text" name="catName" style="border-radius: 0" class="form-control" id="cat-name"
                               required="required"
                               data-bv-field="name">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" name="categoryAdd"
                       style="border-radius: 0" class="btn btn-primary">Add Category</button>
            </div>
                </form>
        </div>

    </div>
</div>

<!-- Modal add subcategory-->
<div class="modal fade" id="subcategoryModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add SubCategory</h4>
            </div>
            <div class="modal-body" style="height: 180px">
                <form action="#" method="post" enctype="multipart/form-data" class="col-md-10">
                    <div class="form-group">
                        <label for="category">Category *</label>
                        <select class="category-select full-size" style="width: 100%" name="categoryId">
                            <?php for ($i = 0; $i < count($categories); $i++) { ?>

                                <option
                                    value="<?= $categories[$i]['cat_id'] ?>"> <?= $categories[$i]['cat_name'] ?></option>

                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">SubCategory Name *</label>
                        <input type="text" name="subName" style="border-radius: 0" class="form-control" id="sub-name"
                               required="required"
                               data-bv-field="name">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" name="subcategoryAdd"
                       style=" border-radius: 0" class="btn btn-primary">Add SubCategory</button>
            </div>
                </form>
        </div>

    </div>
</div>
<script>

    $(document).ready(function () {
        $(".btn-click").click(function () {
            $(".bar").toggle(500);
        });


        $(".category-select").select2({
            placeholder: "Select a category",
            allowClear: true
        });
        var id = $(".category-select").val();
        $.ajax({
            url: "get_subcategory.php",
            type: "post",
            dataType: "json",
            data: {'id': id},
            async: false,
            success: function (result) {
                console.log(result);
                html = "";
                for (var i = 0; i < result.length; i++) {
                    html = html + "<option value=" + result[i]["sub_id"] + ">" + result[i]['sub_name'] + "</option>";
                }
                document.getElementById("sub-select").innerHTML = html;
            }
        });
        $(".category-select").change(function () {
            var id = $(".category-select").val();
            $.ajax({
                url: "get_subcategory.php",
                type: "post",
                dataType: "json",
                data: {'id': id},
                async: false,
                success: function (result) {
                    console.log(result);
                    html = "";
                    for (var i = 0; i < result.length; i++) {
                        html = html + "<option value=" + result[i]["sub_id"] + ">" + result[i]['sub_name'] + "</option>";
                    }
                    document.getElementById("sub-select").innerHTML = html;
                }
            });
        });

    });

    $(function () {
        $(".category-select").select2({
            placeholder: "Select a category",
            allowClear: true
        });
    });

    function readURL(input, img_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + img_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar").change(function () {
        readURL(this, "blah");
    });
    $("#img-1").change(function () {
        readURL(this, "blah1");
    });
    $("#img-2").change(function () {
        readURL(this, "blah2");
    });
    $("#img-3").change(function () {
        readURL(this, "blah3");
    });
    $("#img-4").change(function () {
        readURL(this, "blah4");
    });
    $("#img-5").change(function () {
        readURL(this, "blah5");
    });
    $("#img-6").change(function () {
        readURL(this, "blah6");
    });
</script>