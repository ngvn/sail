<?php
require("../models/Product.php");
require("../layout/index.php");

$created_by = $_SESSION["idUser"];

if (isset($_POST["categoryAdd"])) {
    $name = test_input($_POST["categoryName"]);
    addCategory($name, $created_by);
}

if (isset($_POST["subcategoryAdd"])) {
    $name = test_input($_POST["subName"]);
    $category_id = test_input($_POST["categoryId"]);
    addSubcategory($name, $category_id, $created_by);
}

if (isset($_POST["btnDelete"])) {
    $id = $_POST['id'];
    deleteUser($id);
    deleteOrderByUserId($id);
}

$sub_categories = getAllSubcategory();


?>


<meta charset="UTF-8">
<title>Categories</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/product.css">
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">
<script type="text/javascript" src="../../public/js/jquery.dataTables.min.js"></script>

<style>
    table{
        text-align: center;
        font-size: small;
    }
</style>


<div class="content">
    <div class="category-list">
        <div class="box-header">
            <div class="col-xs-6">
                <a href="index.php"><h2 class="blue"><i class="glyphicon glyphicon-chevron-left"></i>Back &nbsp;</h2></a>
            </div>
            <div class="col-xs-6"> <h2 class="blue">Categories</h2></div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered" id="list-categories">
                <thead>
                <tr>
                    <th style="text-align: center">Category</th>
                    <th style="text-align: center">SubCategory</th>
                    <th style="text-align: center">Created By</th>
                    <th style="text-align: center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($sub_categories); $i++) { ?>
                <tr>

                    <td><?php echo getNameCategoryById($sub_categories[$i]['category_id']) ?></td>
                    <td><?php echo $sub_categories[$i]['sub_name']; ?></td>
                    <td><?php echo getNameCreated($sub_categories[$i]['created_by']); ?></td>
                    <td width="10%">
                        <form method="post" action="#" id="delete-form" style="display: inline">
                            <input type="hidden" name="id" value="<?= $sub_categories[$i]['id']; ?>">

                            <button class="btn btn-danger" name="btnDelete" type="submit" style="font-size: x-small"
                                    value="delete"
                                    onclick="return confirm('Are you sure to delete?');">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>

                    </td>

                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#list-categories').DataTable();
    });
</script>