<?php
require("../models/Customer.php");
require("../layout/index.php");

if (isset($_POST["customerAdd"])) {
    $name = $_POST["name"];
    $phone = $_POST["phone"];
    $address = $_POST["address"];
    $img = $_POST["avatar"];
    if (empty($phone)) {
        $phone = "";
    }
    if (empty($address)) {
        $address = "";
    }
    if (empty($img)) {
        $img = '../../upload/img/avatar-default.jpg';
    }

    createCustomer($name, $phone, $address);
}

if(isset($_POST["btnDelete"])){
    $id = $_POST["id"];
    deleteCustomer($id);
}

$customer = getAllCustomer();
?>

<meta charset="UTF-8">
<title>Customers</title>
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/customer.css">
<script src="../../public/js/jquery.dataTables.min.js"></script>

<div class="content" style="">
    <div class="box-header col-md-12 col-sm-12" style="margin-bottom: 10px;">
        <div class="col-md-6 col-sm-6"><a href="../site/index.php" style="margin-left: -15px">
                <h2 class="blue" style=" margin-left: -20px">
                    <span class=" glyphicon glyphicon-chevron-left"></span>Home &nbsp;</h2>
            </a>
        </div>
        <div class="col-md-5 col-sm-5"><h2 class="blue">Customer &nbsp;</h2></div>
        <div class="col-md-1 col-sm-1">
            <a href="add.php"> <h2 class="blue" style=" margin-left: 160px">
                <span data-toggle="tooltip" title="Add" data-placement="left"
                      class="glyphicon glyphicon-plus"></span></h2></a>
        </div>
    </div>
    <div class="box-content">
        <table class="table table-bordered" id="list-customers">
            <thead>
            <tr>
                <th style="text-align: center" width="10%">Avatar</th>
                <th style="text-align: center" width="25%">Name</th>
                <th style="text-align: center" width="15%">Phone</th>
                <th style="text-align: center" width="43%">Address</th>
                <th style="text-align: center" width="7%">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < count($customer); $i++) { ?>
                <tr>
                    <td style="text-align: center"><img src="<?php echo $customer[$i]['url'] ?>" width="25" height="25"></td>
                    <td><?php echo $customer[$i]['name']; ?></td>
                    <td><?php echo $customer[$i]['phone']; ?></td>
                    <td><?php echo $customer[$i]['address']; ?></td>
                    <td><form method="post" action="#" style="display: inline">
                            <input type="hidden" name="id" value="<?= $customer[$i]['id']; ?>">

                            <button class="btn btn-danger" name="btnDelete" type="submit" style="font-size: x-small"
                                    value="delete"
                                    onclick="return confirm('Are you sure to delete?');">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#list-customers').DataTable();
        $(".popover-menu").click(function () {
            $(".menu-content").toggle();
        });
    });
</script>