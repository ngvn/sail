<?php
require("../layout/index.php");
require("../models/Discount.php");
require("../models/Product.php");

$products = getAllProducts();
?>

<title>Create Customer</title>
<meta charset="UTF-8">
<link href="../../public/css/metro.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/discount.css">
<script src="../../public/js/select2.min.js"></script>
<script>
</script>


<div class="content">
    <div class="box-header">
        <a href="index.php" class="col-md-5" style="margin-left: -15px"><h2 class="blue"><span
                    class="glyphicon glyphicon-chevron-left"></span>Customers &nbsp;</h2>
        </a>

        <div href="#" class="col-md-6" style="margin-left: 15px;"><h2 class="blue">Add Customer &nbsp;</h2></div>
        <a href="add_product.php" class="col-md-1" style=""><h2 class="blue">
                <span class=""></span></h2>
        </a>
    </div>

    <div class="box-content" id="box-content" style="padding: 20px;">
        <div class="">
            <p class="introtext"><span style="margin-left: 40px">Please fill in the information below. The field labels marked with * are required input
                fields.</span></p>
        </div>
        <div class="">
            <form action="index.php" method="post" class="col-md-8">
                <div class="form-group">
                    <label>Name *</label>
                    <input type="text" name="name" value="" class="form-control" id="cate-name"
                           required="required"
                           data-bv-field="promotions">
                </div>
                <div class="form-group">
                    <label style="margin-top: 15px">Phone</label>
                    <input type="text" name="phone" min="0" max="99" class="form-control" id="discount"
                           data-bv-field="promotions">
                </div>
                <div class="form-group">
                    <label style="margin-top: 15px">Address</label>
                    <input type="text" name="address" min="0" max="99" class="form-control" id="discount"
                           data-bv-field="promotions">
                </div>
                <div class="form-group">
                    <label>Avatar</label>
                    <input type="file" name="avatar"
                           data-bv-field="promotions">
                </div>
                <div class="form-group">
                    <input type="submit" name="customerAdd" value="Add Customer"
                           style="border: 1px solid #00aff0; border-radius: 0; color: #00aff0" class="btn btn-default">
                    <a href="index.php"><input type="button" value="Cancel" style="border: 1px solid #f0210e; border-radius: 0; color: #f0210e"
                                               class="btn btn-default"></a>
                </div>
            </form>
        </div>
    </div>
</div>




