<?php

require("config/db.php");
require("config/set_session.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $user = $_POST["user"];
    $pass = $_POST["pass"];
    $org_id = $_POST["org"];
    get_user($user, $pass, $org_id);

}

function get_user($user, $pass, $org_id)
{
    GLOBAL $conn;
    if (isset($user) && isset($pass) && isset($org_id)) {
        $user = addslashes($user);
        $pass = (addslashes(md5($pass)));
        $org_id = addslashes($org_id);
    }
    $sql = mysqli_query($conn, "SELECT * FROM user WHERE user_name='$user' AND pass_word='$pass' AND organization_id='$org_id'");

    if (mysqli_num_rows($sql)>0) {

        $row = mysqli_fetch_array($sql);

        $_SESSION["idUser"] = $row["id"];

        $_SESSION["currUser"] = $row["user_name"];

        $_SESSION["nameUser"] = $row["name"];

        if ($row["role"] == "admin") {

            $_SESSION["currAdmin"] = $row["role"];
            header("location: admin/site/index.php");
        } else {

            header("location: site/index.php");
        }
    } else {

        echo "<script> alert('Thong tin mat khau hoac tai khoan khong chinh xac')</script>";
    }
}

function getAllOrganization(){
    GLOBAL $conn;
    $sql = mysqli_query($conn, "SELECT * FROM organization");
    $result = [];
    if (!empty($sql)) {
        while ($row = mysqli_fetch_array($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}


$org = getAllOrganization();

?>


<link href="public/css/metro.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="public/css/login.css">
<script src="public/js/metro.js"></script>
<script src="public/js/select2.min.js"></script>

<script>

    $(function(){
        $(".org-login").select2({
            placeholder: "Select a Organization",
            allowClear: true
        });
    });
</script>
<title>Login</title>
<meta charset="UTF-8">
<div class="login-form">
    <h3><b>Press login to continue</b></h3>

    <div class="login-header"></div>
    <form action="" method="POST">


        <input type="text" name="user" id="login-user" placeholder="UserName" style="border-radius: 0"> <br>
        <input type="password" name="pass" id="login-pass" placeholder="Passworld" style="border-radius: 0"> <br>
        <p style="margin: 10px 0 0 0; font-weight: bold">Organization:</p>
        <select class="org-login full-size" name="org" style="width: 100%">
            <?php for($i=0; $i<count($org); $i++){?>

                <option value="<?= $org[$i]['org_id']?>"><?= $org[$i]['org_name']?></option>

            <?php }?>
        </select>
        <button type="submit" class="btn btn-primary btn-login" style="border: 1px solid #0050ef">Login</button>
    </form>
</div>