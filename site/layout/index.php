<?php
require("../models/Layout.php");
if (isset($_POST["btnChange"])) {
    $id = $_SESSION["idUser"];
    $name = $_POST["nameChange"];
    $pass = md5($_POST["passChange"]);
    $date = $_POST["dateChange"];
    $email = $_POST["emailChange"];
    $add = $_POST["addressChange"];

    changeInfo($id, $name, $date, $email, $add, $pass);
}

$users = getInfoUserById($_SESSION["idUser"]);

?>


<meta charset="UTF-8">
<link rel="icon" href="../../upload/img/logo/logo1.png" type="image/png">
<link href="../../public/css/metro.css" rel="stylesheet">
<script src="../../public/js/metro.js"></script>
<link rel="stylesheet" type="text/css" href="../css/layout.css">

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header" style="margin-left: -5px;">


            <div class="navbar-brand dropdown tooltip-menu">
                <a id="menu-btn" data-toggle="dropdown" class="navbar-brand" href="#"><img
                        src="../../upload/img/logo/logo2.png"
                        style="width: 100px; height:45px; margin-top: -12px">
                </a>
                <!-- Menu-->
                <div class="menu-content dropdown-menu tooltip-content2"
                     style="background-color: honeydew; margin-left: -50px">
                    <div class="menu-content2">
                        <div class="col-md-2 col-xs-2 item">
                            <a href="../pos/index.php">
                                <img src="../../upload/img/layout/pos.png" width="50px" height="50px"><br>

                                <p style="text-align:center">POS</p>
                            </a>
                        </div>

                        <div class="col-md-2 col-xs-2 item">
                            <a href="../report/report_form.php">
                                <img src="../../upload/img/layout/report.png" width="50px" height="50px"><br>

                                <p style="text-align:center">Reports</p>
                            </a>
                        </div>
                        <div class="col-md-2 col-xs-2 item">
                            <a href="../home/index.php">
                                <img src="../../upload/img/layout/home.png" width="50px" height="50px"><br>
                                <p style="text-align:center">Home</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <ul class="nav navbar-nav">
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown" data-toggle="modal" >
                <a data-toggle="dropdown" href="#" style="margin-right: 40px">
                    <img src="<?= $users['url']?>" width="40px" height="40px" style="border-radius: 20px;">
                    <?= $users['name'] ?>&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu" style="width: 200px; margin-right: 40px;">
                    <li><a href="../user/edit_user.php"><span class="glyphicon glyphicon-pencil"></span> Change Profile</a></li>
                    <li class="divider"></li>
                    <li style="margin-right: 30px; margin-top: 5px"><a href="../../logout.php"><span
                                class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>


<!-- Modal change -->
<div class="modal fade" id="modalChange" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color: #F2F4F7">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Change profile
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form class="form-horizontal" role="form" method="POST" method="#">
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Name
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="text" name="nameChange" value="" class="form-control" id="name-change">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Birthday
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="date" name="dateChange" value="" class="form-control" id="date-change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Email
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="email" name="emailChange" value="" class="form-control" id="email-change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-3 col-md-3 col-lg-2 control-label ">Address
                            :</label>

                        <div class="col-sm-9 col-md-9 col-lg-10">
                            <input type="type" name="addressChange" value="" class="form-control"
                                   id="address-change">
                        </div>
                    </div>
                    <br><br>

                    <h4 style="color: #489EE7">User Login Info</h4>
                    <hr>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-4 col-md-4 col-lg-3 control-label ">Password
                            :</label>

                        <div class="col-sm-8 col-md-8 col-lg-9">
                            <input type="password" name="passChange" value="" class="form-control" id="pass-change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="required col-sm-4 col-md-4 col-lg-3 control-label ">Password
                            Again
                            :</label>

                        <div class="col-sm-8 col-md-8 col-lg-9">
                            <input type="password" name="confirmPass" value="" class="form-control" id="pass-confirm">
                        </div>
                    </div>
                    <br><br>

                    <div class="input-modal">
                        <button type="submit" class="btn btn-primary" name="btnChange"
                                style="width: 100%; background-color: #489EE7">Submit
                        </button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
