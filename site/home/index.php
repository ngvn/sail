<?php

require("../models/Layout.php");
require("../models/User.php");
include('../../public/class/SimpleImage.php');
global $conn;

if (isset($_POST["btnUpdate"])) {
    $name = $_POST["nameEdit"];
    $date = str_replace('.', '-', $_POST["dateEdit"]);
    $add = $_POST["addressEdit"];
    $id = $_SESSION["idUser"];
    updateUser($id, $name, $add, $date);


    if (!empty($_FILES['avatarEdit']['name'])) {

        $file_loc = $_FILES['avatarEdit']['tmp_name'];
        $file = "employee".$id.'.jpg'; //t�n ?nh
        $url = "../../upload/employee/" . $file;
        move_uploaded_file($file_loc, $url);
        if($_FILES['avatarEdit']['size'] > 102400) {
            $image = new SimpleImage();
            $image->load($url);
            $image->resize(250, 400);
            $image->save($url);
        }
    }
}

$users = getInfoUserById($_SESSION["idUser"]);
?>

<head>
    <title>Home</title>
    <link rel="icon" href="../../upload/img/logo/logo1.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="../css/site.css">
</head>

<body style="overflow-x: hidden;">
<div class="body">
    <div class="body-header">
        <div class="btn-home"><a class="navbar-brand" href="#"><img src="../../upload/img/logo/logo.png"
                                                                    style="width: 100px; height:45px; margin-top: -12px"></a>
        </div>
        <div class="btn-change-info">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a data-toggle="dropdown" href="#" style="color: white; margin-right: 40px">
                        <img src="<?= $users['url'] ?>" width="40px" height="40px" style="border-radius: 20px;">
                        <?= $users['name'] ?>&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu" style="width: 150px; margin-right: 40px;">
                        <li><a href="../user/edit_user.php"><span class="glyphicon glyphicon-pencil"></span> Change
                                Profile</a></
                        <li class="divider"></li>
                        <li class="divider"></li>
                        <li style="margin-right: 30px; margin-top: 5px"><a href="../../logout.php"><span
                                    class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <div class="body-content container">
        <div class="container col-xs-12">
            <div class="col-xs-2"></div>
            <div class="col-xs-4">
                <div class=" item">
                    <a href="../pos/index.php">
                        <img src="../../upload/img/layout/pos.png" width="150px" height="150px"><br>

                        <p style="text-align:center">POS</p>
                    </a>
                </div>
            </div>

            <div class="col-xs-4">
                <div class=" item">
                    <a href="../report/report_form.php">
                        <img src="../../upload/img/layout/report.png" width="150px" height="150px"><br>

                        <p style="text-align:center">Reports</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-2"></div>
        </div>
    </div>
</div>
</body>
