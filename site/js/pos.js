var data = [];
var data2 = [];
var customers = [];
$(document).ready(function () {

    $.ajax({
        url: "../pos/get_product_by_category.php",
        type: "post",
        dataType: "json",
        async: false,
        success: function (result) {

            data = result;
        }
    });

    $.ajax({
        url: "../pos/get_items_product.php",
        type: "post",
        dataType: "json",
        async: false,
        success: function (result) {
            data2 = result;
        }
    });

    $.ajax({
        url: "../customer/get_customer.php",
        type: "post",
        dataType: "json",
        async: false,
        success: function (result) {
            customers = result;
        }
    });

    $('#subcategory').paginate({itemsPerPage: 5});
    $('#category').paginate({itemsPerPage: 5});

    $(".btn-zoom-in").click(function () {
        $(".zoom-in").hide();
        $(".zoom-out").show();
    });

    $(".btn-zoom-out").click(function () {
        $(".zoom-out").hide();
        $(".zoom-in").show();
    });


    //customer
    $("#client-select").change(function () {

    });

    $(".btn-prod").click(function () {
        var remail = 0;
        // Su kien payment
        $(".payment").click(function () {

            var oldTotal = parseFloat($("#total").text().replace(/[,  £ $ ¥]/g, ''));
            var discount = 0;
            var total = oldTotal;
            $("#discount").change(function () {
                if ($("#discount").val()) {
                    discount = parseFloat($("#discount").val());
                }
                else
                    discount = 0;
                total = oldTotal - oldTotal * discount / 100;
                remail = total;

                document.getElementById("total3").innerHTML = "<p id='total3'>" + total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</p>";
                document.getElementById("remail").innerHTML = "<p id='remail'>" + remail.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</p>";
                document.getElementById("cash").value = "";
                document.getElementById("creditcard").value = "";
                document.getElementById("change").innerHTML = "<p id='change'>" + 0 + "</p>";
                document.getElementById("discount-total").value = discount;
                document.getElementById("total-amount").value = total;
            });

            remail = total;
            document.getElementById("remail").innerHTML = "<p id='remail'>" + remail.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</p>";

            $("#cash").change(function () {
                var cash = 0;
                var creditcard = 0;
                if ($("#cash").val())
                    cash = parseFloat($("#cash").val());
                if ($("#creditcard").val())
                    creditcard = parseFloat($("#creditcard").val());

                remail = total - cash - creditcard;
                if (remail < 0) {
                    var change = 0 - remail;
                    document.getElementById("remail").innerHTML = "<p id='remail'>" + 0 + "</p>";
                    document.getElementById("change").innerHTML = "<p id='change'>" + change.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</p>";
                }
                else {
                    document.getElementById("remail").innerHTML = "<p id='remail'>" + remail.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</p>";
                    document.getElementById("change").innerHTML = "<p id='change'>" + 0 + "</p>";
                }
            });
            $("#creditcard").change(function () {
                var cash = 0;
                var creditcard = 0;
                if ($("#cash").val())
                    cash = parseFloat($("#cash").val());
                if ($("#creditcard").val())
                    creditcard = parseFloat($("#creditcard").val());

                remail = total - cash - creditcard;

                if (remail < 0) {
                    var change = 0 - remail;
                    document.getElementById("remail").innerHTML = "<p id='remail'>" + 0 + "</p>";
                    document.getElementById("change").innerHTML = "<p id='change'>" + change.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</p>";
                }
                else {
                    document.getElementById("remail").innerHTML = "<p id='remail'>" + remail.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</p>";
                    document.getElementById("change").innerHTML = "<p id='change'>" + 0 + "</p>";
                }
            });

            //get customerID


            var html = "";
            var charArr = "";
            var discountTotal = 0;
            var e = document.getElementById("client-select");
            var cus_id = 1;
            if (e.options[e.selectedIndex].value)
                cus_id = e.options[e.selectedIndex].value;
            if ($("#discount-total").val()) {
                discountTotal = parseFloat($("#discount-total").val());
            }
            for (var i = 0; i < arr.length; i++) {
                if ($(id1 + ">.name")) {
                    if (i == arr.length - 1)
                        charArr = charArr + arr[i];
                    else
                        charArr = charArr + arr[i] + ",";
                    var id1 = "#item-" + arr[i];
                    var name1 = $(id1 + ">.name").text();
                    var nameItem = "priceItem" + arr[i];
                    var price1 = parseFloat($(id1 + ">.price").text().replace(/[,  £ $ ¥]/g, ''));
                    var discount1 = parseFloat($(id1 + ">.discount").text().replace(/[,  £ $ ¥]/g, ''));
                    var qty1 = parseFloat($("#qty-" + arr[i]).val());
                    var subtotal1 = parseFloat($(id1 + ">.subtotal").text().replace(/[,  £ $ ¥]/g, ''));
                    html = html + "<input type='hidden' name=" + "priceItem" + arr[i] + " value=" + price1 + ">" +
                        "<input type='hidden' name=" + "qtyItem" + arr[i] + " value=" + qty1 + ">" +
                        "<input type='hidden' name=" + "idItem" + arr[i] + " value=" + arr[i] + ">" +
                        "<input type='hidden' name=" + "discountItem" + arr[i] + " value=" + discount1 + ">" +
                        "<input type='hidden' name=" + "totalItem" + arr[i] + " value=" + subtotal1 + ">";
                }
            }

            document.getElementById("order-payment").innerHTML = html + "<input type='hidden' name='charArr' value=" + charArr + ">" +
                "<input type='hidden' name='discountTotal' id='discount-total' value=" + discountTotal + ">" +
                "<input type='hidden' name='customerId' id='customer-id' value=" + cus_id + ">" +
                "<input type='hidden' name='totalAmount' id='total-amount' value=" + oldTotal + ">";
        });
    });
});


// show subcategory and product after click button category
function showSubCategoryList(value) {

    var html = "", html2 = "";

    //$("#" + item_id).remove();
    for (var i = 0; i < data.length; i++) {

        if (data[i]["cat_id"] == value) {
            //kiem tra da ton tai sub chua
            if (html.search("subcategory-" + data[i]["sub_id"]) == -1) {
                //console.log($("#subcategory-"+data[i]["sub_id"]).text() == null);
                //subcategory
                html = html + "<li style='display: inline'>" +
                    "<button id='subcategory-" + data[i]["sub_id"] + "' type='button' value='' class='btn-prni active' " +
                    "onclick='showProductList(" + data[i]['sub_id'] + ")'>" +
                    "<span style='font-size: x-small; font-weight: bold'>" + data[i]["sub_name"] + "</span>" +
                    "</button></li>";
            }
            if (data[i]["price"] != null) {
                html2 = html2 + "<li>" +
                    "<div id='product-" + data[i]["pro_id"] + "' type='button' value='' class='btn-prod product btn-prni  active'>" +
                    "<img src='" + data[i]["url"] + "' onclick='showItemslist(" + data[i]['pro_id'] + ")'>" +
                    "<p id='product-name'>" + data[i]["pro_name"] + "</p>" +
                    "<p><b>" + data[i]["price"].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</b>&nbsp;||&nbsp;" +
                    "<i class='glyphicon glyphicon-info-sign' onclick='showInfoProduct(" + data[i]['pro_id'] + ")' data-toggle='modal' data-target='#productInfo'></i>" +
                    "</p>" +
                    "</div></li><div id='modal'></div>";
            }

        }
    }
    document.getElementById("subcategory").innerHTML = html;

    document.getElementById("list-li-items").innerHTML = html2;
}


// show product after click button subcategory
function showProductList(value) {

    var html = "";

    for (var i = 0; i < data.length; i++) {
        if (data[i]["sub_id"] == value && data[i]["price"]!=null) {
            html = html + "<li style='display: inline'>" +
                "<div id='product-" + data[i]["pro_id"] + "' type='button' value='' class='btn-prod product btn-prni active'>" +
                "<img src='" + data[i]["url"] + "' onclick='showItemslist(" + data[i]['pro_id'] + ")'>" +
                "<p id='product-name'>" + data[i]["pro_name"] + "</p>" +
                "<p><b>" + data[i]["price"].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</b>&nbsp;||&nbsp;" +
                "<i class='glyphicon glyphicon-info-sign' onclick='showInfoProduct(" + data[i]['pro_id'] + ")' data-toggle='modal' data-target='#productInfo'></i>" +
                "</p>" +
                "</div></li><div id='modal'></div>";
        }
    }
    document.getElementById("list-li-items").innerHTML = html;
}


//show items after click button product
var total = 0;
var ad = 0;
var qty = 1;
var arr = [];


function showItemslist(value) {

    var oldTotal = total;
    var subtotal = 0;
    var result = [];
    var product_id = value;
    for (var i = 0; i < data2.length; i++) {
        if (data2[i]["pro_id"] == value) {
            result.push(data2[i]);
            break;
        }
    }

    var item_id = "item-" + result[0]["pro_id"];
    var jquery_item_id = "#item-" + result[0]["pro_id"];
    var delete_id = "delete-" + result[0]["pro_id"];
    var jquery_delete_id = "#" + delete_id;
    var price = result[0]["price"];
    var qty_id = "qty-" + result[0]["pro_id"];
    var jquery_qty_id = "#" + qty_id;
    var subtotal_id = "subtotal-" + result[0]["pro_id"];
    var jquery_subtotal_id = "#" + subtotal_id;

    var discount = 0;
    var discount2 = 0;
    var currency = result[0]["symbol"];
    if (result[0]["discount"]) {
        discount = result[0]["discount"];
    }
    if (document.getElementById(item_id) != null) {

        var oldQty = parseFloat($(jquery_qty_id).val());
        var oldSub = price * oldQty - price * oldQty * discount / 100;
        total = total - oldSub;
        qty = document.getElementById(qty_id).value;
        qty = parseFloat(qty) + 1;
        subtotal = price * qty - price * qty * discount / 100;
        total = total + subtotal;
        document.getElementById(qty_id).value = parseFloat(document.getElementById(qty_id).value) + 1;
        document.getElementById(subtotal_id).innerHTML = "<td id='" + subtotal_id + "'>" + subtotal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</td>";
    }
    else {
        arr.push(result[0]["pro_id"]);
        qty = 1;
        subtotal = price * qty - price * qty * discount / 100;

        var html = "<tr id='" + item_id + "' style='text-align: center'><td class='name'>" + result[0]["pro_name"] + "</td>" +
            "<td class='price'>" + price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</td>" +
            "<td class='qty'><input type='number' onkeypress='return event.charCode >= 48 && event.charCode <= 57' min='1' id='" + qty_id + "' value='" + qty + "' style='text-align: center; width: 50px; height: 20px'></td>" +
            "<td class='discount'>" + discount + "%</td>" +
            "<td class='subtotal' id='" + subtotal_id + "'>" + subtotal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</td>" +
            "<td><a href='#'><span class='glyphicon glyphicon-remove' id='" + delete_id + "'></span></a></td></tr>";
        $('#list-products').append(html);
        total = total + subtotal;
    }
    $(jquery_qty_id).change(function () {
        if (parseFloat($(this).val()) % 1 != 0 || parseFloat($(this).val()) < 1) {
            alert("You must enter a number!");
            document.getElementById(qty_id).value = 1;
        }
        var oldSub = parseFloat($(jquery_subtotal_id).text().replace(/[, £ $ ¥]/g, ''));
        total = total - oldSub;
        qty = parseFloat(document.getElementById(qty_id).value);
        subtotal = price * qty - price * qty * discount / 100;
        total = total + subtotal;
        document.getElementById(subtotal_id).innerHTML = "<td class='subtotal' id='" + subtotal_id + "'>" + subtotal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "</td>";
        document.getElementById('total').innerHTML = "<b id='total'>" + total + "</b>";
        document.getElementById("total2").innerHTML = "<b id='total2' class='col-xs-7'>" + total + "</b>";
        document.getElementById("total3").innerHTML = "<td id='total3'>" + total + "</td>";
        setValueTotal(total, currency);
    });

    $(".reset").click(function () {
        total = 0;
        setValueTotal(total, currency);
        document.getElementById("list-products").innerHTML = "";
    });

    $(jquery_delete_id).click(function () {
        if ($(jquery_qty_id).val()) {
            var oldQty = parseFloat($(jquery_qty_id).val());
            subtotal = price * oldQty - price * oldQty * discount / 100;
        }
        else subtotal = 0;
        total = total - subtotal;
        document.getElementById("total").innerHTML = "<b>" + total + currency + "</b>";
        document.getElementById("total2").innerHTML = "<b id='total2' class='col-xs-7'>" + total + currency + "</b>";
        document.getElementById("total3").innerHTML = "<td id='total3'>" + total + "</td>";
        $("#" + item_id).remove();
        setValueTotal(total, currency);
    });

    setValueTotal(total, currency);
}

function setValueTotal(total, currency) {
    total = formatNumber(total, currency);
    if (currency == '₫') {
        document.getElementById("total").innerHTML = "<td id='total'>" + total + " " + currency + "</td>";
        document.getElementById("total2").innerHTML = "<b id='total2' class='col-xs-7'>" + total + currency + "</b>";
        document.getElementById("total3").innerHTML = "<td id='total3'>" + total + "</td>";
    }
    else {
        document.getElementById("total").innerHTML = "<td  id='total'> " + currency + " " + total + "</td>";
        document.getElementById("total2").innerHTML = "<b id='total2' class='col-xs-7'>" + total + currency + "</b>";
        document.getElementById("total3").innerHTML = "<td id='total3'>" + total + "</td>";
    }
}


//modal info Product
function showInfoProduct(value) {

    document.getElementById("modal").innerHTML = "";

    var data = [];
    for (var i = 0; i < data2.length; i++) {
        if (data2[i]["pro_id"] == value) {
            data.push(data2[i]);
        }
    }

    var content = '';
    var header = '<div class="modal fade" id="productInfo" tabindex="-1" role="dialog">' +
        '<div class="modal-dialog" style="width: 70%">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal">' +
        '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
        '<h4 class="modal-title" id="myModalLabel">' + data[0]["pro_name"] + '</h4></div>' +
        '<div class="modal-body">' +
        '<div id="owl-image">';
    if (data[0]['url']) {
        if (data[0]['discount']) {
            for (var i = 1; i < data.length; i++) {
                content = content + '<div class="item"><img src="' + data[i]['url'] + '" style ="width: 300px; height: 300px"></div>';
            }
        }
        else
            for (var i = 0; i < data.length; i++) {
                content = content + '<div class="item"><img src="' + data[i]['url'] + '" style ="width: 300px; height: 300px"></div>';
            }
    }
    else {
        content = '<div class="item"><img src="../../upload/img/no_image.png" style ="width: 300px; height: 300px"></div>';
    }
    var footer = '</div><br>' + '<div><b>Price: </b>' + data[0]['price'].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' <b>' + data[0]['symbol'] + '</b><br><br>' +
        '<b>Code: </b>' + data[0]['pro_code'] + '<br><br>' +
        '<b>Description: </b>' + data[0]['description'] + '<br><br>' + '</div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">' +
        'Close</button></div></div></div></div>';

    var modal = header + content + footer;

    document.getElementById("modal").innerHTML = modal;

    $("#owl-image").owlCarousel({

        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3]
    });
}


function formatNumber(n, a) {

    var t = [];

    if (a == '₫') {
        n = Number(n).toFixed(0).split('.');
    }
    else n = Number(n).toFixed(2).split('.');

    for (var iLen = n[0].length, i = iLen ? iLen % 3 || 3 : 0, j = 0; i <= iLen; i += 3) {
        t.push(n[0].substring(j, i));
        j = i;
    }
    return t.join(',') + (n[1] ? '.' + n[1] : '');
}

/*
 $("#client-select").change(function () {
 var e = document.getElementById("client-select");
 var id = e.options[e.selectedIndex].value;
 for (var i = 0; i < customers.length; i++) {
 if (customers[i]["customer_id"] == parseFloat(id)) {
 document.getElementById("client-img").innerHTML = "<img src='" + customers[i]["url"] + "'>";
 break;
 }
 }

 });
 */
