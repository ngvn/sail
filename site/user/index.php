<?php
require("../models/User.php");
require("../layout/index.php");
include('../../public/class/SimpleImage.php');
global $conn;
if (isset($_POST["btnCreate"])) {
    $name = $_POST["nameCreate"];
    $user = $_POST["userCreate"];
    $pass = md5($_POST["passCreate"]);
    $date = str_replace('.', '-', $_POST["dateCreate"]);
    $email = $_POST["emailCreate"];
    $add = $_POST["addressCreate"];
    $role = $_POST["roleCreate"];
    $org = 1;

    createUser($name, $user, $pass, $date, $add, $email, $role, $org);
    $employee_id = mysqli_insert_id($conn);

    if (!empty($_FILES['avatar']['name'])) {
        $file_loc = $_FILES['avatar']['tmp_name'];
        $file = "employee".$employee_id.'.jpg'; //t�n ?nh
        $url = "../../upload/employee/" . $file;
        move_uploaded_file($file_loc, $url);
        if($_FILES['avatar']['size'] > 102400) {
            $image = new SimpleImage();
            $image->load($url);
            $image->resize(250, 400);
            $image->save($url);
        }
        createAvatar($file, $employee_id, $url);
    }
    else{
        $file = "employee".$employee_id.'.jpg';
        $url = "../../upload/employee/avatar-default.jpg";
        createAvatar($file, $employee_id, $url);
    }
}


if (isset($_POST["btnUpdate"])) {
    $name = $_POST["nameEdit"];
    $date = str_replace('.', '-', $_POST["dateEdit"]);
    $add = $_POST["addressEdit"];
    $id = $_SESSION["idUser"];
    updateUser($id, $name, $add, $date);


    if (!empty($_FILES['avatarEdit']['name'])) {

        $file_loc = $_FILES['avatarEdit']['tmp_name'];
        $file = "employee".$id.'.jpg'; //t�n ?nh
        $url = "../../upload/employee/" . $file;
        move_uploaded_file($file_loc, $url);
        if($_FILES['avatarEdit']['size'] > 102400) {
            $image = new SimpleImage();
            $image->load($url);
            $image->resize(250, 400);
            $image->save($url);
        }
    }
}

if (isset($_POST["btnDelete"])) {
    $id = $_POST['id'];
    deleteUser($id);
    deleteAvatar($id);
    // delete c�c user
}

$users = getInfoUser();
$org = getAllOrganization();
?>


<title>Users</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">

<script type="text/javascript" src="../../public/js/jquery.dataTables.min.js"></script>
<script src="../../public/js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/user.css">

<script>

    $(function () {
        $(".org-select").select2({
            placeholder: "Select a Organization",
            allowClear: true
        });
    });
</script>

<div class="content">
    <div class="box-header" style="margin-bottom: 10px;">
        <a href="../site/index.php" class="col-md-6" style="margin-left: -15px">
            <h2 class="blue">
                <span class="glyphicon glyphicon-chevron-left"></span>Home &nbsp;
            </h2>
        </a>

        <div class="col-md-5" style="margin-left: 0px;"><h2 class="blue">Employees &nbsp;</h2></div>
        <div class="col-md-1 col-sm-1">
            <h2 class="blue" style=" margin-left: 80px">
                <a href="add.php" name="userCreate">
                    <span data-toggle="tooltip" title="Add" data-placement="left"
                        class="glyphicon glyphicon-plus"></span>
                </a>
            </h2>
        </div>
    </div>
    <div class="box-content">
        <table class="table table-striped table-bordered" id="list-users" style="font-size: small">
            <thead>
            <tr>
                <th style="width: 3%; text-align: center"><b>Avatar</b></th>
                <th style="text-align: center;"><b>Name</b></th>
                <th style="text-align: center"><b>Email</b></th>
                <th style="text-align: center"><b>Username</b></th>
                <th style="text-align: center"><b>Authority</b></th>
                <th style="width: 5%; text-align: center">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < count($users); $i++) { ?>
                <tr>
                    <td style="text-align: center" width="20%"><img src="<?= $users[$i]['url']?>" width="30px" height="30px"></td>
                    <td width="20%"><?php echo $users[$i]['name']; ?></td>
                    <td width="25%"><?php echo $users[$i]['email']; ?></td>
                    <td width="15%" style="text-align: center"><?php echo $users[$i]['user_name']; ?></td>
                    <td width="10%" style="text-align: center"><?php echo $users[$i]['role']; ?></td>
                    <td style="text-align: center" width="10%">
                        <form method="post" action="#" id="delete-form" style="display: inline">
                            <input type="hidden" name="id" value="<?= $users[$i]['id']; ?>">

                            <button class="btn btn-danger" name="btnDelete" type="submit" style="font-size: x-small"
                                    value="delete"
                                    onclick="return confirm('Are you sure to delete?');">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>

                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<script>

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#list-users').DataTable();
        $(".popover-menu").click(function () {
            $(".menu-content").toggle();
        });
    });
</script>