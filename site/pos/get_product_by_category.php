<?php
require("../../config/db.php");

global $conn;

$sql = mysqli_query($conn, "SELECT p.pro_id, p.pro_name, p.price, p.description, i.url, p.pro_code,
                          i.url, c.symbol, s.sub_id, s.sub_name, cat.cat_id, cat_name
                    FROM product p
                    JOIN currency_item c
                    ON p.currency_id = c.id
                    JOIN image_product i
                    ON p.pro_id = i.product_id
                    JOIN subcategory s
                    ON p.subcategory_id = s.sub_id
                    JOIN category cat
                    ON s.category_id = cat.cat_id
                    WHERE p.active = 1 AND i.avatar = 1");
$result = [];
if (!empty($sql)) {
    while ($row = mysqli_fetch_assoc($sql)) {
        $result[] = $row;
    }
}

$sql = mysqli_query($conn, "SELECT s.sub_id, s.sub_name, cat.cat_id, cat.cat_name
                    FROM subcategory s
                    JOIN category cat
                    ON s.category_id = cat.cat_id");
$result2 = [];
if (!empty($sql)) {
    while ($row = mysqli_fetch_assoc($sql)) {
        $result2[] = $row;
    }
}

$data = array_merge($result, $result2);
echo json_encode($data);
?>