<script>
    var id;

</script>

<?php
require("../models/Product.php");
require("../models/Pos.php");
require("../models/Customer.php");
require("../layout/index.php");
include("../../public/class/SimpleImage.php");



global $conn;

if (isset($_POST["customerAdd"])) {
    $name = $_POST["name"];
    $phone = $_POST["phone"];
    $address = $_POST["address"];
    if (empty($phone)) {
        $phone = "";
    }
    if (empty($address)) {
        $address = "";
    }
    if (empty($img)) {
        $img = '../../upload/img/avatar-default.jpg';
    }

    createCustomer($name, $phone, $address);

    $customer_id = mysqli_insert_id($conn);
    if (!empty($_FILES['avatar']['name'])) {
        $avatar = $_FILES['avatar'];
        $file_loc = $_FILES['avatar']['tmp_name'];
        $file = strstr($_FILES['avatar']['name'], '.');
        $file = $customer_id . strstr($file, '.');
        $url = "../../upload/customer/" . $file;
        $type = 1;
        move_uploaded_file($file_loc, $url);
        if ($_FILES['avatar']['size'] > 102400) {

            $image = new SimpleImage();
            $image->load($url);
            $image->resize(250, 400);
            $image->save($url);
        }
        createImageCustomer($file, $customer_id, $url);
    } else {
        $file = "avartar-default.jpg";
        $url = "../../upload/img/avatar-default.jpg";
        createImageCustomer($file, $customer_id, $url);
    }
}
$arr = [];
if (isset($_POST["payment"])) {

    $salesman_id = $_SESSION["idUser"];
    $temp = $_POST["charArr"];
    $customer_id = (int)$_POST["customerId"];
    $date_created = date("Y/m/d");
    $discountTotal = (int)$_POST["discountTotal"];
    $totalAmount = (float)$_POST["totalAmount"];
    createOrder($date_created, $customer_id, $salesman_id, $discountTotal, $totalAmount);
    $order_id = mysqli_insert_id($conn);
    array_push($arr, explode(",", $temp));
    for ($i = 0; $i < count($arr[0]); $i++) {
        $product_id = $_POST["idItem" . $arr[0][$i]];
        $price = $_POST["priceItem" . $arr[0][$i]];
        $qty = $_POST["qtyItem" . $arr[0][$i]];
        $discountItem = $_POST["discountItem" . $arr[0][$i]];
        $total = $_POST["totalItem" . $arr[0][$i]];

        createOrderline($order_id, $product_id, $qty, $price, $discountItem, $total);
    }

}

$customers = getAllCustomer();

$img_customers = getAllImageCustomer();

$cate = getAllCategory();

$sub = getSubCategoryOfFirstCategory();

$discount = getAllDiscountHaveTime();

$products = getItemsOfFirstCategory();
?>

<head>
    <meta charset="utf-8">
    <title>POS</title>
    <link href="../../public/css/metro.css" rel="stylesheet">
    <script src="../../public/js/owl.carousel.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../public/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="../../public/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="../css/pos.css">
    <script type="text/javascript" src="../../public/js/jquery.paginate.min.js"></script>
    <script src="../../public/js/select2.min.js"></script>
    <script src="../js/pos.js"></script>

    <script>

        $(function () {
            $(".client-select").select2({
                placeholder: "Select a client",
                allowClear: true
            });
        });
    </script>
</head>

<div class="content2" style="overflow-x: hidden;">
    <div class="col-xs-12 box-header">
        <div class="col-xs-6">
            <a href="../home/index.php"><h2 class="blue"><span
                        class="glyphicon glyphicon-chevron-left"></span>Home &nbsp;</h2>
            </a>
        </div>

        <div href="#" class="col-xs-6"><h2 class="blue">POS &nbsp;</h2></div>
    </div>
    <div class="pos">

        <div class="left-div" id="print">
            <div class="left-middle">
                <table class="table table-striped table-bordered table-fixed">
                    <thead style="border-bottom: 0">
                    <tr>
                        <th class="col-xs-3">Item</th>
                        <th class="col-xs-2">Price</th>
                        <th class="col-xs-2">Qty</th>
                        <th class="col-xs-2">Discount</th>
                        <th class="col-xs-2">Subtotal</th>
                        <th class="col-xs-1"><span class="glyphicon glyphicon-trash"></span></th>
                    </tr>
                    </thead>

                    <tbody id="list-products">

                    </tbody>
                </table>
            </div>

            <div class="left-bottom">

                <div class="zoom-in">
                    <div class="btn-zoom-in"><span id="btn-down" class="glyphicon glyphicon-chevron-down"></span></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-10 "><br>

                        <select class="client-select" id="client-select" name="clientId" style="width: 350px">
                            <?php for ($i = 0; $i < count($customers); $i++) { ?>
                                <option
                                    value="<?= $customers[$i]['id'] ?>"><?= $customers[$i]['name'] ?></option>
                            <?php } ?>
                        </select>
                        <a href="#" data-toggle="modal" data-target="#customerModal">
                            <span class="glyphicon glyphicon-user icon-customer" data-toggle="tooltip"
                                  title="Add Customer!"></span>
                        </a><br><br>

                        <div class="calculator col-xs-12">
                            <br><br>

                            <div>
                                <b class="col-xs-5" style="margin-left: -15px">Total:</b>
                                <b id="total" class="col-xs-7">0</b>
                            </div>
                        </div>

                        <div class="col-md-12 btn-action">
                            <div class="col-xs-5" style="padding: 0;">
                                <button type="button" class="btn btn-warning btn-block btn-flat cyan bot print"
                                        style="border: 1px solid #f0ad4e"
                                        onClick="window.print(); return false;" tabindex="-1">
                                    Print
                                </button>
                                <button type="button" class="btn btn-danger btn-block btn-flat reset"
                                        style="border: 1px solid #d43f3a; margin-top: -1px;" tabindex=" -1">
                                    Cancel
                                </button>
                            </div>

                            <div class="col-xs-7" style="padding: 0;">

                                <button type="button" class="btn btn-success btn-block payment"
                                        style=" height:67px; border: 1px solid #449d44" data-toggle="modal"
                                        data-target=".paymentModal"
                                        tabindex="-1">
                                    <i class="fa fa-money" style="margin-right: 5px;"></i>Payment
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="zoom-out">
                    <div class="btn-zoom-out"><span class="glyphicon glyphicon-chevron-up"></span></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <div>
                            <br>
                            <b class="col-xs-5" style="margin-left: -15px">Total:</b>
                            <b id="total2" class="col-xs-7">0</b>
                        </div>
                        <div style="margin-top: 30px">
                            <button type="button" class="btn btn-danger col-xs-3 reset"
                                    style="border: 1px solid #d43f3a;" tabindex=" -1">
                                Cancel
                            </button>
                            <button type="button" class="btn btn-warning col-xs-3 print"
                                    style="border: 1px solid #f0ad4e"
                                    onClick="window.print(); return false;" tabindex="-1">
                                Print
                            </button>
                            <button type="button" class="btn btn-success col-xs-3 payment"
                                    style=" border: 1px solid #449d44" tabindex="-1" data-toggle="modal"
                                    data-target=".paymentModal">
                                Payment
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="right">
            <div class="right-div">
                <div class="right-category">
                    <ul id="category" class="ps-container" style="margin:0 0 10px 10px; ">
                        <?php for ($i = 0; $i < count($cate); $i++) { ?>
                            <li style="display: inline">
                                <button id="<?= 'category-' . $cate[$i]['cat_id'] ?>" type="button"
                                        value="<?= $cate[$i]['cat_name'] ?>"
                                        onclick="showSubCategoryList(<?= $cate[$i]['cat_id'] ?>)"
                                        class="btn-prni  active" tabindex="-1">
                                    <span
                                        style='font-size: x-small; font-weight: bold'><?= $cate[$i]['cat_name'] ?></span>
                                </button>
                            </li>

                        <?php } ?>
                        <br><br>
                    </ul>

                    <div id="category-pagination">
                        <a id="category-previous" href="#"><img src="../../upload/img/next.png" width="30px"
                                                                height="30px"></a>
                        <a id="category-next" href="#"><img src="../../upload/img/next.png" width="30px"
                                                            height="30px"></a>
                    </div>
                </div>
                <div class="right-content">
                    <div class="right-subcategory">
                        <ul id="subcategory" class="ps-container" style="margin:0 0 10px 10px; ">
                            <?php for ($i = 0; $i < count($sub); $i++) { ?>
                                <li style="display: inline; margin-left: -4px">
                                    <button id="<?= 'subcategory-' . $sub[$i]['sub_id'] ?>" type="button"
                                            value="<?= $sub[$i]['sub_id'] ?>"
                                            onclick="showProductList(<?= $sub[$i]['sub_id'] ?>)"
                                            class="btn-prni  active" tabindex="-1">
                                        <span
                                            style='font-size: x-small; font-weight: bold'><?= $sub[$i]['sub_name'] ?></span>
                                    </button>
                                </li>

                            <?php } ?>
                            <br><br>
                        </ul>

                        <div id="subcategory-pagination">
                            <a id="subcategory-previous" href="#"><img src="../../upload/img/next.png" width="30px"
                                                                       height="30px"></a>
                            <a id="subcategory-next" href="#"><img src="../../upload/img/next.png" width="30px"
                                                                   height="30px"></a>
                        </div>

                    </div>


                    <div class="list-items" id="list">
                        <ul id="list-li-items">
                            <?php for ($i = 0; $i < count($products); $i++) { ?>
                                <li>
                                    <div class="btn-prod product btn-prni active">
                                        <img src="<?= $products[$i]["url"] ?>"
                                             alt="<?= $products[$i]['pro_name'] ?>"
                                             onclick="showItemslist(<?= $products[$i]['pro_id'] ?> )"
                                             class="img-rounded">

                                        <p id="product-name"><?= $products[$i]['pro_name'] ?></p>

                                        <p><b><?= number_format($products[$i]['price'], 2, '.', ',') ?></b>&nbsp;||&nbsp;
                                            <i class="glyphicon glyphicon-info-sign"
                                               onclick="showInfoProduct(<?= $products[$i]['pro_id']; ?>)"
                                               data-toggle="modal" data-target="#productInfo">
                                            </i>
                                        </p>

                                    </div>
                                </li>

                            <?php } ?>
                            <div id="modal"></div>
                        </ul>
                    </div>


                    <div class="slide-discount">
                        <marquee onmouseout="this.start();" onmouseover="this.stop();">

                            <table id="list-li-discount">
                                <tbody>
                                <tr>
                                    <?php if (count($discount) > 0) for ($i = 0; $i < count($discount); $i++) {
                                        $price_after = $discount[$i]['price'] - $discount[$i]['price'] * $discount[$i]['discount'] / 100; ?>
                                        <td style="border-right: 3px solid white">
                                            <div class="product-discount">
                                                <div class="col-xs-5" style="position: relative">
                                                    <img src="<?= $discount[$i]['url'] ?>"
                                                         alt="<?= $discount[$i]['dis_name'] ?>"
                                                         style="width:90px;height:90px; margin-top: 5px">

                                                    <div id="btn-sale"><span>-<?= $discount[$i]["discount"] ?>%</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-7" style="font-size: small;">

                                                    <p style=" margin-top: 5px">
                                                        <?= $discount[$i]['pro_name'] ?><br>
                                                        <strike><?php if ($discount[$i]['symbol'] == '₫') echo " " . number_format($discount[$i]['price']) . " " . $discount[$i]['symbol'] . " ";
                                                            else echo " " . $discount[$i]['symbol'] . " " . number_format($price_after, 2, '.', ',') . " " ?></strike><br>
                                                        <?php if ($discount[$i]['symbol'] == '₫') echo " " . number_format($price_after) . " " . $discount[$i]['symbol'] . " ";
                                                        else echo " " . $discount[$i]['symbol'] . " " . number_format($price_after, 2, '.', ',') . " " ?>
                                                    </p>

                                                </div>
                                            </div>
                                        </td>
                                    <?php } ?>
                                </tbody>
                                </tr>
                            </table>
                        </marquee>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal add customer-->
<div class="modal fade" id="customerModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Customer</h4>
            </div>
            <div class="modal-body" style="height: 380px">
                <form action="#" method="post" enctype="multipart/form-data" class="col-md-10">
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" name="name" value="" class="form-control" id="cate-name"
                               required="required"
                               data-bv-field="promotions">
                    </div>
                    <div class="form-group">
                        <label style="margin-top: 15px">Phone</label>
                        <input type="text" name="phone" min="0" max="99" class="form-control"
                               data-bv-field="promotions">
                    </div>
                    <div class="form-group">
                        <label style="margin-top: 15px">Address</label>
                        <input type="text" name="address" min="0" max="99" class="form-control"
                               data-bv-field="promotions">
                    </div>
                    <div class="form-group">
                        <label>Avatar</label>
                        <input type="file" name="avatar" id="avatar"
                               data-bv-field="promotions">
                    </div>
                    <div class="form-group">
                        <input type="submit" name="customerAdd" value="Add Customer"
                               style="border: 1px solid #00aff0; border-radius: 0; color: #00aff0"
                               class="btn btn-default">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal payment-->
<div class="modal fade paymentModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Payment</h4>
            </div>

            <div class="modal-body" style="height:220px">
                <form action="#" method="post" enctype="multipart/form-data" class="col-md-12">

                    <div class="form-group">
                        <label class="col-xs-3">Discount:</label>
                        <input type="number" value=0 id="discount" min="0" max="100" style=""> %
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3">Total:</label>

                        <p id="total3"> 0</p>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3">Remail:</label>

                        <p id="remail"> 0.00</p>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Cash:</label>
                            <input name="cash" type="text" id="cash" class="form-control"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                   style="border-radius: 0px">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>CreditCard:</label>
                            <input name="creditcard" type="text" id="creditcard" class="form-control"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                   style="border-radius: 0px">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3">Change:</label>

                        <p id="change">0</p>
                    </div>
                    <div id="order-payment"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="payment" class="btn btn-primary addPayment">Payment</button>
            </div>
            </form>
        </div>

    </div>
</div>

<script>

    $('[data-toggle="tooltip"]').tooltip();
</script>


