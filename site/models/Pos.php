<?php

function getAllOrderLine()
{
    global $conn;
    $sql = mysqli_query($conn, "SELECT * FROM order_line");
    $result = [];
    if ($sql) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $result[] = $row;
        }
    }
    return $result;
}

function createOrder($date_created, $business_partner_id, $salesman_id, $discount, $total)
{
    global $conn;
    $business_partner_id = addslashes($business_partner_id);
    $salesman_id = addslashes($salesman_id);
    $discount = addslashes($discount);
    $total = addslashes($total);

    $sql = mysqli_query($conn, "INSERT INTO item_order ( date_created, business_partner_id, salesman_id, discount, total)
                                VALUES ('$date_created', '$business_partner_id', '$salesman_id', '$discount', '$total')");


    return $sql;
}

function createOrderline($order_id, $product_id, $qty, $price, $discount, $total)
{
    global $conn;
    $order_id = addslashes($order_id);
    $product_id = addslashes($product_id);
    $qty = addslashes($qty);
    $price = addslashes($price);
    $discount = addslashes($discount);
    $total = addslashes($total);

    $sql = mysqli_query($conn, "INSERT INTO order_line (order_id, product_id, qty, price, discount, total)
                                      VALUES ('$order_id', '$product_id', '$qty', '$price', '$discount', '$total')");

    return $sql;
}

