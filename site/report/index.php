<?php
require("../models/Report.php");
require("../layout/index.php");

if (isset($_POST["btnReport"])) {
    $from = str_replace('.', '-', $_POST["fromDate"]);
    $to = str_replace('.', '-', $_POST["toDate"]);
    $employee = $_POST["employeeId"];
    $orders = getOrderByValue($from, $to, $employee, "");
    $total = 0;
    $amount = 0;
    for($i=0;$i<count($orders); $i++){
        $amount = $amount + $orders[$i]["total"];
        $total = $total + $orders[$i]['total']/(100-$orders[$i]['discount'])*100;
    }
    $total = number_format((float)$total, 2, '.', '');
    $discount = $total - $amount;
} else {
    echo '<script language="javascript" type="text/javascript"> window.location = "report_form.php";</script>';
}

?>

<meta charset="UTF-8">
<title>Report</title>

<script src="../../public/js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../public/css/jquery.dataTables.min.css">
<script type="text/javascript" src="../../public/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/report.css">

<div class="content">

    <div class="box-header col-md-12 col-sm-12" style="margin-bottom: 10px;">
        <div class="col-md-5 col-sm-5"><a href="../home/index.php" style="margin-left: -15px">
                <h2 class="blue" style=" margin-left: -20px">
                    <span class=" glyphicon glyphicon-chevron-left"></span>Home &nbsp;</h2>
            </a>
        </div>
        <div class="col-md-5 col-sm-5" style="margin-left: 15px;"><h2 class="blue">Report &nbsp;</h2></div>
        <div class="col-md-1 col-sm-1"><h2 class="blue" style=" margin-left: 160px">
                <span data-toggle="tooltip" title="Export" data-placement="left"
                      class="glyphicon glyphicon-export"></span>
            </h2></div>
    </div>
    <div class="box-content">

        <ul class="top-content col-md-12">
            <li class="col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px">
                <div><b>Total: <?= $total;?></b></div>
            </li>
            <li class="col-lg-4 col-md-4 col-sm-4">
                <div><b>Discount: <?= $discount;?></b></div>
            </li>
            <li class="col-lg-4 col-md-4 col-sm-4" style="padding-right: 5px">
                <div><b>Total Amount: <?= $amount;?></b></div>
            </li>
        </ul>
        <div class="col-md-12" style="margin: -10px 0px 0px 3px ">
            <table class="table table-striped table-bordered" id="list-orders" style="font-size: small">
                <thead>
                <tr>
                    <th style="text-align: center" width="30%">Employee</th>
                    <th style="text-align: center" width="20%">Date</th>
                    <th style="text-align: center" width="20%">Total</th>
                    <th style="text-align: center" width="10%">Discount</th>
                    <th style="text-align: center" width="20%">Amount</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($orders); $i++) { ?>
                    <tr>
                        <td><?= getNameEmployee($orders[$i]['salesman_id']) ?></td>
                        <td style="text-align: center"><?= $orders[$i]['date_created'] ?></td>
                        <td style="text-align: center"><?= number_format((float)$orders[$i]['total']/(100-$orders[$i]['discount'])*100, 2, '.', '') ?></td>
                        <td style="text-align: center"><?= $orders[$i]['discount'] ?> %</td>
                        <td style="text-align: center"><?= $orders[$i]['total'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    $(function () {
        $("#from").datepicker();
        $("#to").datepicker();
    });
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $(".employee-select").select2({
            placeholder: "Select a category",
            allowClear: true
        });
        $('#list-orders').DataTable();
    });

</script>