<?php

require("config/db.php");
session_start();
if(!$_SESSION["currUser"]){

    header("location: login.php");
} else{

    if($_SESSION["currAdmin"]){
        header("location: admin/site/index.php");
    }
    else{
        header("location: site/home/index.php");
    }
}